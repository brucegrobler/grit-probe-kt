WIP 



# Build 

maven clean package

# Execute

java -jar target/grit-probe-1.0.0-SNAPSHOT.jar



Deploy to repo: :

bgrobler@grit-server:~$ aptly repo add -force-replace gritlabs-release ~/grit-probe-1.2.3-RELEASE.deb

## Example pub/sub for voltronic command

# mosquitto_pub -h 10.1.0.1 -u user -P 'password' -t
"grit/probe/production/grobler/voltronic_inverter/command/QMOD/request" -m "QMOD"

# mosquitto_sub -h 10.1.0.1 -u "user" -P "password" -t "grit/probe/production/grobler/+/command/+/#" -v -d

Client mosq-AcQ1rwfrRGQaWccUtu received PUBLISH (d0, q0, r0, m0, 'grit/probe/production/grobler/voltronic_inverter/command/QMOD/request', ... (4 bytes))
grit/probe/production/grobler/voltronic_inverter/command/QMOD/request QMOD
Client mosq-AcQ1rwfrRGQaWccUtu received PUBLISH (d0, q0, r0, m0, 'grit/probe/production/grobler/voltronic_inverter/command/QMOD/response', ... (1 bytes))
grit/probe/production/grobler/voltronic_inverter/command/QMOD/response L
