package io.gritlabs.probe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GritProbe {

	public static void main(String[] args) {
		SpringApplication.run(GritProbe.class, args);
	}
}

