package io.gritlabs.probe.domain.repository.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity(name = "coulombs")
@Getter
@Setter
@NoArgsConstructor
public class CoulombEntity {

	Double        ampHours = 0.0;
	ZonedDateTime created  = ZonedDateTime.now();
	ZonedDateTime deleted  = null;
	@Id
	@GeneratedValue(generator = "UUID")
	@Column(name = "id", nullable = false)
	UUID id;
	Double soc = 0.0;

	@Column(unique = true)
	String name = null;
	ZonedDateTime updated = ZonedDateTime.now();

	public CoulombEntity(UUID id, ZonedDateTime created, ZonedDateTime updated, ZonedDateTime deleted, String name,
	                     Double ampHours, Double soc) {

		this.id = id;
		this.created = created;
		this.updated = updated;
		this.deleted = deleted;
		this.name = name;
		this.ampHours = ampHours;
		this.soc = soc;
	}

}
