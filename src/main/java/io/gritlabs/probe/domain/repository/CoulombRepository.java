package io.gritlabs.probe.domain.repository;

import io.gritlabs.probe.domain.repository.entity.CoulombEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CoulombRepository extends PagingAndSortingRepository<CoulombEntity, String> {

	public Optional<CoulombEntity> findByName(String name);
}
