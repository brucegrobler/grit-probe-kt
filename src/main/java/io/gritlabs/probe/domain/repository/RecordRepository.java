package io.gritlabs.probe.domain.repository;

import io.gritlabs.probe.domain.repository.entity.RecordEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RecordRepository extends PagingAndSortingRepository<RecordEntity, String> {

	public List<RecordEntity> findAllByOrderByCreatedDesc(Pageable pageRequest);
}
