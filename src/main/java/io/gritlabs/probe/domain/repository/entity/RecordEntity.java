package io.gritlabs.probe.domain.repository.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity(name = "records")
@Getter
@Setter
@NoArgsConstructor
public class RecordEntity {

	ZonedDateTime created = ZonedDateTime.now();
	ZonedDateTime deleted = null;
	@Id
	@GeneratedValue(generator = "UUID")
	@Column(name = "id", nullable = false)
	UUID id = UUID.randomUUID();
	ZonedDateTime updated = ZonedDateTime.now();

	public RecordEntity(UUID id, ZonedDateTime created, ZonedDateTime deleted, ZonedDateTime updated,
	                    String telemetry) {

		this.id = id;
		this.created = created;
		this.deleted = deleted;
		this.updated = updated;
		this.telemetry = telemetry;
	}

	@Column(length = 65536)
	String telemetry = null;
}
