package io.gritlabs.probe.domain.service;

public interface EventDispatcherService {

	public void dispatch(String topic, String payload);

}
