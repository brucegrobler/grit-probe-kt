package io.gritlabs.probe.domain.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.gritlabs.probe.domain.repository.RecordRepository;
import io.gritlabs.probe.domain.repository.entity.RecordEntity;
import io.gritlabs.probe.domain.service.dto.PointDto;
import io.gritlabs.probe.domain.service.dto.PointTypeDto;
import io.gritlabs.probe.domain.service.dto.TelemetryDto;
import io.gritlabs.probe.domain.service.model.RecordModel;
import io.gritlabs.probe.util.Util;
import io.gritlabs.probe.util.VersionHolder;
import lombok.SneakyThrows;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@Slf4j
class TelemetrySinkServiceImpl implements TelemetrySinkService {

	private final Integer                  bufferReadMax;
	private final Boolean                  enableBuffer;
	private final ExecutorService          executor = Executors.newCachedThreadPool(new CustomizableThreadFactory(
			"sink-"));
	private final Boolean                  homeAssistantCompat;
	private final HttpWebClientServiceImpl httpWebClient;
	private final MqttService              mqttService;
	private final ObjectMapper             objectMapper;
	private final RecordRepository         repository;
	private final Boolean                  sendMqttTelemetryDto;
	private final VersionHolder            versionHolder;

	public TelemetrySinkServiceImpl(@Autowired MqttService mqttService,
	                                @Autowired ObjectMapper objectMapper,
	                                @Autowired HttpWebClientServiceImpl httpWebClient,
	                                @Value("${grit.instanceId:INVALID}") String instanceId,
	                                @Value("${grit.buffer.enabled:true}") Boolean enableBuffer,
	                                @Value("${grit.buffer.read.max:10}") Integer bufferReadMax,
	                                @Autowired VersionHolder versionHolder,
	                                @Autowired RecordRepository repository,
	                                @Value("${grit.sendMqttTelemetryDto:true}") Boolean sendMqttTelemetryDto,
	                                @Value("${grit.homeAssistantCompat:false}") Boolean homeAssistantCompat) {

		this.mqttService = mqttService;
		this.objectMapper = objectMapper;
		this.httpWebClient = httpWebClient;
		this.enableBuffer = enableBuffer;
		this.bufferReadMax = bufferReadMax;
		this.versionHolder = versionHolder;
		this.repository = repository;
		this.sendMqttTelemetryDto = sendMqttTelemetryDto;
		this.homeAssistantCompat = homeAssistantCompat;
	}

	@SneakyThrows
	private RecordEntity mapRecordEntity(RecordModel recordModel) {

		return new RecordEntity(recordModel.getId(), recordModel.getCreated(), recordModel.getUpdated(),
				recordModel.getDeleted(), objectMapper.writeValueAsString(recordModel.getTelemetry()));
	}

	@SneakyThrows
	private RecordModel mapRecordModel(RecordEntity recordEntity) {

		return new RecordModel(recordEntity.getId(), recordEntity.getCreated(), recordEntity.getUpdated(),
				recordEntity.getDeleted(), objectMapper.readValue(recordEntity.getTelemetry(), TelemetryDto.class));
	}

	private void persist(List<RecordModel> records) {

		if (!enableBuffer) {
			return;
		}

		repository.saveAll(records.stream().map(it -> {
			return new RecordEntity(it.getId(), it.getCreated(), it.getUpdated(), it.getDeleted(),
					it.getTelemetry().toJson());
		}).collect(Collectors.toList()));
		log.debug("Buffered [" + records.size() + "]");
	}

	@SneakyThrows
	public void publish(TelemetryDto telemetry) {

		log.debug("TelemetrySink Publish");

		telemetry.getAdditionalData().put("version", versionHolder.getVersion());
		telemetry.getAdditionalData().put("hostname", Util.getHostname());

		executor.submit(() -> {
			try {
				mqttService.publish(telemetry.getName() + "/status", telemetry.getStatus());

				if (homeAssistantCompat) {
					for (PointDto point : telemetry.getMeasurement().getPoints()) {
						mqttService.publish(telemetry.getName() + "/telemetry/" + point.getName(),
								point.getValue().toString());
					}
				} else if (sendMqttTelemetryDto) {
					mqttService.publish(telemetry.getName() + "/telemetry",
							objectMapper.writeValueAsString(telemetry));
				}
			} catch (Exception e) {
				log.error("MQTT Sink Exception: " + e.getMessage(), e);
				persist(Collections.singletonList(new RecordModel(UUID.randomUUID(), ZonedDateTime.now(),
						ZonedDateTime.now(),
						null, telemetry)));
			}
		});

		executor.submit(() -> {
			try {
				httpWebClient.publish(telemetry);
			} catch (RuntimeException e) {
				log.error("Web Sink Exception: " + e.getMessage());
				persist(Collections.singletonList(new RecordModel(UUID.randomUUID(), ZonedDateTime.now(),
						ZonedDateTime.now(),
						null, telemetry)));
			}
		});
		log.debug("TelemetrySink Publish Complete");
	}

	private void purge(List<RecordModel> records) {

		repository.deleteAll(records.stream()
				.map(this::mapRecordEntity)
				.collect(Collectors.toList()));

		log.debug("Deleted Records [" + records.stream().map(recordModel -> recordModel.getId() + ", ").collect(Collectors.toList()) + "]");
	}

	private void purge(String id) {

		repository.deleteById(id);
		log.debug("Deleted Record [" + id + "]");
	}

	private List<RecordModel> read() {

		LinkedList<RecordModel> records = new LinkedList<>();

		List<RecordEntity> pagedRecords = repository.findAllByOrderByCreatedDesc(PageRequest.of(0,
				bufferReadMax));

		log.debug("Retrieved [" + pagedRecords.size() + "] from buffer");

		pagedRecords.forEach(it -> {
			records.add(mapRecordModel(it));
		});

		log.debug("Mapped [" + records.size() + "] from buffer");

		return records;
	}

	@Synchronized
	@Scheduled(initialDelay = 30000, fixedDelay = 60000)
	public void readBuffer() {

		if (!enableBuffer) {
			return;
		}

		try {
			List<RecordModel> records = read();
			if (records.isEmpty()) {
				log.debug("No Buffered Records");
				return;
			}

			purge(records);

			records.forEach(it -> {
				log.debug("Publish Cached Record");
				try {
					it.getTelemetry().getMeasurement().getPoints().add(
							new PointDto("BufferTransmissionDelta",
									ChronoUnit.MILLIS.between(it.getCreated(), ZonedDateTime.now()),
									PointTypeDto.INT32));
					this.publish(it.getTelemetry());
				} catch (Exception e) {
					log.error("Buffer Publish Exception", e);
				}
			});

		} catch (Exception e) {
			log.warn("Read Exception (Buffer) - " + e.getMessage(), e);
		}
	}

}

