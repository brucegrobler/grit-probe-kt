package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.service.dto.TelemetryDto;

public interface TelemetrySinkService {

	public void publish(TelemetryDto telemetry);
}
