package io.gritlabs.probe.domain.service.model;

import com.google.common.eventbus.Subscribe;

public interface EventListener {

	@Subscribe
	public void onEventReceived(EventRequestModel eventRequestModel);
}
