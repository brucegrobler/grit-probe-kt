package io.gritlabs.probe.domain.service.dto;

public enum PointTypeDto {
   STRING,
   INT32,
   INT64,
   DECIMAL,
   BOOLEAN;
}
