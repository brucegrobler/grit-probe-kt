package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.service.dto.TelemetryDto;

public interface HttpWebClientService {

	void publish(TelemetryDto telemetry);
}
