package io.gritlabs.probe.domain.service.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
public class TelemetryDto {


	public  String         status;
	private LinkedHashMap  additionalData;
	private String         instanceId;
	private MeasurementDto measurement;
	private String         name;
	@Autowired
	private ObjectMapper   objectMapper;

	public TelemetryDto(String instanceId, String name, MeasurementDto measurement) {

		this.instanceId = instanceId;
		this.name = name;
		this.measurement = measurement;
		additionalData = new LinkedHashMap();
	}

	@SneakyThrows
	public String toJson() {

		return objectMapper.writeValueAsString(this);
	}

	public String toString() {

		return "TelemetryDto(instanceId=" + this.instanceId + ", name=" + this.name + ", measurement=" + this.measurement + ")";
	}

}
