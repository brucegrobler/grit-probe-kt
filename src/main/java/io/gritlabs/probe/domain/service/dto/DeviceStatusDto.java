package io.gritlabs.probe.domain.service.dto;

import io.gritlabs.probe.domain.service.model.DeviceParametersModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class DeviceStatusDto {

	private String                deviceName;
	private String                driverClass;
	private String                status;
	private DeviceParametersModel parameters;
	private ZonedDateTime         updated;
	private TelemetryDto          telemetry;

}
