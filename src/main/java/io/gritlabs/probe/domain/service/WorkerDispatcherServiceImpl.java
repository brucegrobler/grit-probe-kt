package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.driver.PollingDriver;
import io.gritlabs.probe.domain.driver.Status;
import io.gritlabs.probe.domain.service.model.DeviceModel;
import lombok.SneakyThrows;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.ZonedDateTime;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WorkerDispatcherServiceImpl implements WorkerDispatcherService {

	private final DeviceService          deviceService;
	private final ExecutorService        dispatchExecutor   =
			Executors.newCachedThreadPool(new CustomizableThreadFactory("dispatch" +
					"-"));
	private final EventDispatcherService eventDispatcherService;
	private final Long                   interval;
	private final Long                   maxSemaphoreAttempts;
	private final ExecutorService        supervisorExecutor =
			Executors.newSingleThreadExecutor(new CustomizableThreadFactory(
					"dispatch-supervisor-"));
	private final TelemetrySinkService   telemetrySinkService;
	private final ExecutorService        workerExecutor     =
			Executors.newCachedThreadPool(new CustomizableThreadFactory("worker" +
					"-"));
	private       Boolean                active             = true;

	public WorkerDispatcherServiceImpl(@Autowired DeviceService deviceService,
	                                   @Autowired TelemetrySinkService telemetrySinkService,
	                                   @Value("${grit.scheduler.poll.interval:5000}") Long interval,
	                                   @Value("${grit.scheduler.semaphoreLockAttempts:3}") Long maxSemaphoreAttempts,
	                                   @Autowired EventDispatcherService eventDispatcherService) {

		this.deviceService = deviceService;
		this.telemetrySinkService = telemetrySinkService;
		this.interval = interval;
		this.maxSemaphoreAttempts = maxSemaphoreAttempts;
		this.eventDispatcherService = eventDispatcherService;
	}

	private void checkAndResetDriverFailedLocks(DeviceModel device) {

		if (device.getDriver().getSemaphoreExhaustionCount().get() >= maxSemaphoreAttempts) {
			if (device.getDriver().getSemaphore().availablePermits() < 1) {
				device.getDriver().release();
			}
			device.getDriver().destroy();
			device.getDriver().init();
			log.warn(
					"Device re-initialized due to excessive failed locks/timeouts - " + device.getName()
							+ " permits=[" + device.getDriver().getSemaphore().availablePermits() + "]");
			device.getDriver().getSemaphoreExhaustionCount().set(0);
		}
	}

	@PreDestroy
	public void destroy() {

		this.active = false;
	}

	@SneakyThrows
	@Synchronized
	private void dispatch() {

		try {

			val devices = deviceService.getEnabledDevices().stream().filter(d ->
					d.getDriver().getSemaphore().availablePermits() > 0
			).collect(Collectors.toList());

			while (active) {
				Thread.sleep(interval);
				for (DeviceModel device : devices) {
					log.debug("Polling... (every $interval ms) " + devices.stream().map(d -> d.getName() + ":" + d.getTimeout() + "ms"));

					AtomicReference<Thread> workerThread = new AtomicReference<>(Thread.currentThread());
					try {
						log.debug("Semaphore Acquisition (attempt) - " + device.getName() + " permits=[" + device.getDriver().getSemaphore().availablePermits() + "]");
						checkAndResetDriverFailedLocks(device);

						if (device.getDriver().acquire()) {
							device.setInterval(interval);

							log.info("Semaphore Acquired! - " + device.getName() + " permits=["
									+ device.getDriver().getSemaphore().availablePermits() + "]");

							dispatchExecutor.submit(() -> {
								Future future = null;
								try {
									future = workerExecutor.submit(() -> {
										workerThread.set(Thread.currentThread());
										process(device);
									});

									future.get(device.getTimeout(), TimeUnit.MILLISECONDS);
								} catch (InterruptedException | ExecutionException | TimeoutException e) {
									log.error("Dispatch - Worker[" + workerThread.get().getName() + "] Timeout: [" + device.getName() + "] [" + e.getClass().getSimpleName() + "] (" + e.getMessage() + ")");
									device.getDriver().getSemaphoreExhaustionCount().getAndIncrement();
								} finally {
									if (future != null) {
										future.cancel(true);
									}
								}
							});

							device.getDriver().release();

							log.debug("Released: " + device.getName() + " Worker[" + workerThread.get().getName() + "]");
							log.info("Worker[" + workerThread.get().getName() + "] Complete - " + device.getName() +
									" ref=" + device.hashCode() + "\n [" + device.getDriver().getTelemetry() + "]");

						} else {
							log.warn("No available permits: " + device.getName() + ", acquisition attempts: " + device.getDriver().getSemaphoreExhaustionCount().incrementAndGet());
						}

					} catch (Exception e) {
						log.error("Dispatch Exception Worker[" + workerThread.get().getName() + "] : " + e.getClass().getName() + " " + e.getMessage());
						log.debug("Dispatch Exception Worker[" + workerThread.get().getName() + "] : " + e.getClass().getName() + " " + e.getMessage(), e);
					}
					log.debug("Dispatch Complete - " + device.getName());
				}
			}
		} catch (Exception e) {
			log.error("Dispatcher Exception (Re-Init)", e);
			init();
		}
	}

	@PostConstruct
	public void init() {

		log.debug("Dispatcher initializing...");

		supervisorExecutor.submit(() -> {
			dispatch();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		});

		log.debug("Dispatcher initialized...");
	}

	private void offline(DeviceModel device) {

		device.getDriver().setStatus(Status.OFFLINE);
	}

	private void online(DeviceModel device) {

		device.getDriver().setStatus(Status.ONLINE);
	}

	private void poll(PollingDriver driver, Long timeout) throws Throwable {

		log.debug("Submitted " + driver.getClass().getSimpleName() + " for execution with timeout of " + timeout + "ms" +
				"...");
		driver.process();
	}

	private void polling(DeviceModel device) {

		device.getDriver().setStatus(Status.POLLING);
	}

	private void process(DeviceModel device) {

		log.debug("Processing: " + device.getName());
		try {

			polling(device);
			poll(device.getDriver(), device.getTimeout());
			online(device);
			publish(device);

		} catch (Throwable e) {
			offline(device);
			log.error("Process Exception " + device.getName() + " - [" + e.getMessage() + "]", e);
		} finally {
			device.getDriver().setUpdated(ZonedDateTime.now());
		}
		log.debug("Processing Done: " + device.getName());

	}

	private void publish(DeviceModel device) {

		telemetrySinkService.publish(device.getDriver().getTelemetry());

	}


}
