package io.gritlabs.probe.domain.service.model;

import lombok.Data;

@Data
public class DeviceAction {

	private String description;

	private String displayName;
	private String eventTopic;
	private String eventPayload;

	public DeviceAction(String displayName, String eventTopic, String eventPayload, String description) {

		this.displayName = displayName;
		this.eventTopic = eventTopic;
		this.eventPayload = eventPayload;
		this.description = description;
	}
}
