package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.service.dto.DeviceStatusDto;
import io.gritlabs.probe.domain.service.model.DeviceModel;

import java.util.LinkedList;
import java.util.List;

public interface DeviceService {
	public DeviceModel getDevice(String name);

	public LinkedList<DeviceStatusDto> getDeviceStatus();

	public List<DeviceModel> getDevices();

	public List<DeviceModel> getEnabledDevices();
}
