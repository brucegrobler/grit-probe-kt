package io.gritlabs.probe.domain.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PointDto {
	private String       name;
	private Object       value;
	private PointTypeDto type;
}
