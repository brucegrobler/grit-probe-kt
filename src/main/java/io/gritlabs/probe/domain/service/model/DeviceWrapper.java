package io.gritlabs.probe.domain.service.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "grit")
public class DeviceWrapper {

	public List<DeviceModel> getDevices() {

		return devices;
	}

	public void setDevices(List<DeviceModel> devices) {

		this.devices = devices;
	}

	public String getInstanceId() {

		return instanceId;
	}

	private final String instanceId;
	List<DeviceModel> devices;

	public DeviceWrapper(@Value("${grit.instanceId:INVALID}") String instanceId) {

		this.instanceId = instanceId;
	}

	@Override
	public String toString() {

		return "DeviceWrapper{" +
				"devices=" + devices +
				'}';
	}
}
