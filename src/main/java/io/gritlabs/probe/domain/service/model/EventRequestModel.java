package io.gritlabs.probe.domain.service.model;

import lombok.Data;

@Data
public class EventRequestModel {

	private String payload;
	private String topic;

	public EventRequestModel(String topic, String payload) {

		this.topic = topic;
		this.payload = payload;
	}

}
