package io.gritlabs.probe.domain.service.model;

import io.gritlabs.probe.domain.service.dto.TelemetryDto;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
public class RecordModel {

	private ZonedDateTime deleted;
	private ZonedDateTime created;
	private TelemetryDto  telemetry;
	private UUID          id;
	private ZonedDateTime updated;

	public RecordModel(UUID id, ZonedDateTime created, ZonedDateTime updated, ZonedDateTime deleted,
	                   TelemetryDto telemetry) {

		this.id = id;
		this.created = created;
		this.updated = updated;
		this.deleted = deleted;
		this.telemetry = telemetry;
	}

}
