package io.gritlabs.probe.domain.service;

import com.google.common.eventbus.EventBus;
import io.gritlabs.probe.domain.service.model.EventListener;
import io.gritlabs.probe.domain.service.model.EventRequestModel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
@Getter
public class EventDispatcherServiceImpl implements EventDispatcherService, ApplicationContextAware {

	private final        AdminService       adminService;
	private final        DeviceService      deviceService;
	private static final EventBus           eventBus = new EventBus();
	private              ApplicationContext applicationContext;

	public EventDispatcherServiceImpl(@Autowired AdminService adminService,
	                                  @Autowired DeviceService deviceService) {

		this.adminService = adminService;
		this.deviceService = deviceService;
	}

	public void dispatch(String topic, String payload) {

		if (topic.endsWith("request")) {
			log.debug("Received Dispatch Event ${adminService.instanceId} $topic $payload");
			if ((topic.contains(adminService.getInstanceId()) || topic.contains("broadcast"))) {

				log.debug("Dispatch Event [$topic] [$payload]");
				EventRequestModel event = new EventRequestModel(topic, payload);
				eventBus.post(event);
			}
		} else {
			log.trace("Dispatch received event that is not a request.");
		}
	}

	public static EventBus getEventBus() {

		return eventBus;
	}

	@PostConstruct
	public void registerDeviceListeners() {

		try {

			deviceService.getEnabledDevices().forEach(it -> {
				if (it.getDriver() instanceof EventListener) {
					log.info("Event Listener Registered : " + it.getName());
					eventBus.register(it.getDriver());
				}
			});
		} catch (Exception e) {
			log.warn("Listeners Exception", e);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {

		this.applicationContext = applicationContext;
	}
}
