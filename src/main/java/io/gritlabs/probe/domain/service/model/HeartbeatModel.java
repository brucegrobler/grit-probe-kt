package io.gritlabs.probe.domain.service.model;

import java.time.ZonedDateTime;

public class HeartbeatModel {

	Long dateTime = ZonedDateTime.now().toInstant().toEpochMilli();

	Long   startTime = ZonedDateTime.now().toInstant().toEpochMilli();
	String hostname;
	String version;
	String instanceId;

	public HeartbeatModel(Long startTime, String hostname, String version, Long dateTime, String instanceId) {

		this.startTime = startTime;
		this.hostname = hostname;
		this.version = version;
		this.dateTime = dateTime;
		this.instanceId = instanceId;
	}

}
