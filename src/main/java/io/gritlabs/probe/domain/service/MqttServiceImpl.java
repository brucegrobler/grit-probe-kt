package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.service.model.EventListener;
import io.gritlabs.probe.domain.service.model.EventRequestModel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.fusesource.mqtt.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.URI;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class MqttServiceImpl implements MqttService, EventListener {

	private final Boolean                    enabled;
	private final EventDispatcherServiceImpl eventDispatcherService;
	private final String                     password;
	private final Boolean                    retainMessage;
	private final String                     serverUri;
	private final String                     username;
	private       FutureConnection           connection;
	private       MQTT                       mqtt;
	private       Boolean                    running = true;
	private       String                     topicBase;

	public MqttServiceImpl(
			@Autowired EventDispatcherServiceImpl eventDispatcherService,
			@Value("${grit.mqtt.enabled:false}") Boolean enabled,
			@Value("${grit.mqtt.url:tcp:://localhost:1883}") String serverUri,
			@Value("${grit.mqtt.username:@null}") String username,
			@Value("${grit.mqtt.password:@null}") String password,
			@Value("${grit.mqtt.retain:true}") Boolean retainMessage,
			@Value("${grit.instanceId}") String instanceId,
			@Value("${grit.mqtt.topic.base:grit}") String topicBase
	) {

		this.eventDispatcherService = eventDispatcherService;
		this.enabled = enabled;
		this.serverUri = serverUri;
		this.username = username;
		this.password = password;
		this.retainMessage = retainMessage;
		this.topicBase = topicBase;
	}

	@PreDestroy
	public void destroy() {

		this.running = false;
	}

	@SneakyThrows
	@PostConstruct
	public void init() {

		if (Boolean.FALSE.equals(enabled)) {
			return;
		}
		eventDispatcherService.getEventBus().register(this);
		initTopicBase();

		URI uri = new URI(serverUri);
		mqtt = new MQTT();
		mqtt.setHost(uri.getHost(), uri.getPort());
		mqtt.setUserName(username);
		mqtt.setPassword(password);
		connection = mqtt.futureConnection();

		Executors.newSingleThreadExecutor().submit(() -> {
					try {

						connection.connect().await(30000L, TimeUnit.MILLISECONDS);

						// Subscribe to a topic
						Topic[] topics = new Topic[]{
								new Topic("$topicBase+/command/+/request", QoS.AT_MOST_ONCE),
								new Topic("grit/probe/broadcast", QoS.AT_MOST_ONCE)};

						connection.subscribe(topics).await();
						log.info("MqttService: Subscribe Success " + Arrays.stream(topics).map(t -> t.name().toString()));

						while (running) {
							while (connection.isConnected()) {
								Message message = connection.receive().await();

								log.debug("MqttService: Message Arrived: [" + message.getTopic() + "] [" + new String(message.getPayload()) + "}]");
								Executors.newCachedThreadPool().submit(() -> {
									eventDispatcherService.dispatch(message.getTopic(),
											new String(message.getPayload()));
								});

							}
							Thread.sleep(1000L);
							log.warn("Not Connected, cannot receive messages, waiting 1000ms...");
						}
					} catch (Exception e) {
						Long wait = 10000L;
						log.error("Mqtt Init Exception - [" + serverUri + " ] [" + e.getMessage() + "] waiting + " + wait + "ms");

						try {
							Thread.sleep(wait);
						} catch (InterruptedException ex) {
							throw new RuntimeException(ex);
						}
						init();
					}
				}
		);
	}

	private void initTopicBase() {

		if (!topicBase.endsWith("/")) {
			topicBase = "${this.topicBase}/";
		}
		topicBase = "$topicBase$instanceId/";
	}

	@Override
	public void onEventReceived(EventRequestModel eventRequest) {

		if (eventRequest.getTopic().contains("response") ||
				eventRequest.getTopic().contains("advice")) {
			log.info("MqttService - Event Received [" + eventRequest.getTopic() + "] [" + eventRequest.getPayload() +
					"]");
			publish(eventRequest.getTopic(), eventRequest.getPayload());
		}
	}

	public void publish(String topic, String payload) {

		if (!enabled) {
			return;
		}

		String qualifiedTopic = topic;

		if (!topic.startsWith(topicBase)) {
			qualifiedTopic = "$topicBase$topic";
		}

		if (!(connection.isConnected())) {
			log.error("MqttService: Not Connected - [" + serverUri + "] - Attempting to re-establish. ");
			return;
		}

		try {
			connection.publish(qualifiedTopic, payload.getBytes(), QoS.AT_LEAST_ONCE, retainMessage);
			log.debug("MqttService: Published - [" + qualifiedTopic + "] [" + payload + "]");
		} catch (Exception e) {
			log.error("Publish Exception: [" + e.getMessage() + "]", e);
		}
	}
}
