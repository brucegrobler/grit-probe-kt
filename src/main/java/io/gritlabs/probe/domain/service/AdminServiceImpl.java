package io.gritlabs.probe.domain.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AdminServiceImpl implements AdminService {

	@Value("${grit.instanceId}")
	private String instanceId;

	@Override
	public String getInstanceId() {

		return instanceId;
	}
}
