package io.gritlabs.probe.domain.service.model;

import io.gritlabs.probe.domain.driver.PollingDriver;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class DeviceModel {

	private LinkedList<DeviceAction> actions        = new LinkedList<>();
	private PollingDriver            driver;
	private String                   driverBeanName = "no-driver-set";
	private boolean                  enabled        = false;
	private String                   instanceId     = "no-instance-id-set";
	private Long                     interval       = 0L;
	private String                   name;
	private DeviceParametersModel    parameters     = new DeviceParametersModel();
	private Long                     timeout        = 30000L;

	public void registerActions(List<DeviceAction> actions) {

		this.actions.clear();
		this.actions.addAll(actions);
	}
}
