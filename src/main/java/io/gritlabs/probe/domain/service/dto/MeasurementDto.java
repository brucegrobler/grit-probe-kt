package io.gritlabs.probe.domain.service.dto;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
public class MeasurementDto {

	private long                timeDeltaMillis;
	private long                timestamp;
	private Map<String, Object> pointMap = new LinkedHashMap<>();
	private List<PointDto>      points   = new LinkedList<>();

	public MeasurementDto(long timeDeltaMillis, long timestamp) {

		this.timeDeltaMillis = timeDeltaMillis;
		this.timestamp = timestamp;
	}

	public MeasurementDto(long timeDeltaMillis, long timestamp, List<PointDto> points) {

		this.timeDeltaMillis = timeDeltaMillis;
		this.timestamp = timestamp;
		this.points = points;
		this.pointMap = points.stream().collect(Collectors.toMap(PointDto::getName, PointDto::getValue,
				(val1, val2) -> val2));
	}
	public List<PointDto> getPoints() {

		this.pointMap = points.stream().collect(Collectors.toMap(PointDto::getName, PointDto::getValue,
				(val1, val2) -> val2));

		return this.points;
	}
}
