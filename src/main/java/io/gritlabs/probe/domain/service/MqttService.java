package io.gritlabs.probe.domain.service;

import javax.annotation.PostConstruct;

public interface MqttService {

	String topicBase = null;

	@PostConstruct
	public void init();

	public void publish(String topic, String payload);
}
