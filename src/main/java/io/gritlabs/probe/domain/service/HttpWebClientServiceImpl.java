package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.service.dto.TelemetryDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class HttpWebClientServiceImpl implements HttpWebClientService {

	private final Boolean enabled;
	private final String  serverUrl;
	RestTemplate client = new RestTemplateBuilder()
			.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.build();

	public HttpWebClientServiceImpl(@Value("${grit.web.client.serverUrl:https://cloud.grobler.xyz/grit}") String serverUrl,
	                                @Value("${grit.web.client.enabled:false}") Boolean enabled) {

		this.serverUrl = serverUrl;
		this.enabled = enabled;

	}

	public void publish(TelemetryDto telemetry) {

		if (!enabled) {
			return;
		}
		String url = "$serverUrl/telemetry";

		HttpStatus statusCode = client.postForEntity(url, telemetry, Void.class).getStatusCode();

		log.debug("WebClient [$url] StatusCode=[${statusCode.value()}]");

		if (!statusCode.is2xxSuccessful()) {
			throw new RuntimeException("WebClient Exception - Status: $statusCode");
		}
	}
}
