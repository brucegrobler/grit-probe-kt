package io.gritlabs.probe.domain.service.model;

import lombok.Data;

@Data
public class EventResponseModel {

	public EventResponseModel(String message) {

		this.message = message;
	}

	private String message;
	private String topic;

}
