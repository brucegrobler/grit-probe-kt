package io.gritlabs.probe.domain.service;

import io.gritlabs.probe.domain.driver.PollingDriver;
import io.gritlabs.probe.domain.service.dto.DeviceStatusDto;
import io.gritlabs.probe.domain.service.model.DeviceModel;
import io.gritlabs.probe.domain.service.model.DeviceWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class DeviceServiceImpl implements DeviceService, ApplicationContextAware {

	private final DeviceWrapper            deviceWrapper;
	private final Map<String, DeviceModel> devices = new LinkedHashMap<>();
	private       ApplicationContext       applicationContext;

	public DeviceServiceImpl(@Autowired DeviceWrapper deviceWrapper) {

		this.deviceWrapper = deviceWrapper;
	}

	@Override
	public DeviceModel getDevice(String name) {

		return devices.values().stream().filter(d -> d.getName().equals(name)).findFirst().orElseGet(null);
	}

	@Override
	public LinkedList<DeviceStatusDto> getDeviceStatus() {

		LinkedList<DeviceStatusDto> valueMap = new LinkedList<>();

		devices.values().stream().forEachOrdered(it -> {
					valueMap.add(
							new DeviceStatusDto(
									it.getName(),
									it.getDriver().getClass().getCanonicalName(),
									it.getDriver().getStatus(),
									it.getParameters(),
									it.getDriver().getUpdated(),
									it.getDriver().getTelemetry()
							)
					);
				}
		);

		return valueMap;
	}

	@Override
	public List<DeviceModel> getDevices() {

		return new ArrayList<>(devices.values());
	}

	@Override
	public List<DeviceModel> getEnabledDevices() {

		return devices.values().stream().filter(DeviceModel::isEnabled).collect(toList());
	}

	@PostConstruct
	public void init() {

		deviceWrapper.getDevices().stream().peek(
				it -> {
					it.setInstanceId(deviceWrapper.getInstanceId());
					try {
						it.setDriver((PollingDriver) applicationContext.getBean(it.getDriverBeanName(), it));
						it.getDriver().validate();
						log.info("[" + it.getName() + "] [" + it.getDriver().getClass().getSimpleName() + "]: " +
								"Initialized... config: " + it.getParameters());
					} catch (NoSuchBeanDefinitionException e) {
						log.error("Error Loading Driver [" + it.getName() + "] [" + it.getDriverBeanName() + "] [" + e.getMessage() + "]");
					} catch (Exception e) {
						log.error("Error Loading Driver [" + it.getName() + "] [" + it.getDriverBeanName() + "]", e);
					}
				}
		).forEachOrdered(it -> devices.putIfAbsent(it.getName(), it));
	}

	public void setApplicationContext(ApplicationContext applicationContext) {

		this.applicationContext = applicationContext;
	}
}
