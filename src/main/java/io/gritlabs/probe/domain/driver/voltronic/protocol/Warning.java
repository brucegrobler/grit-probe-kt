package io.gritlabs.probe.domain.driver.voltronic.protocol;

import lombok.Data;

@Data
public class Warning {

	String  description;
	Integer identifier;

	String label;

	public Warning(Integer identifier, String description, String label) {

		this.identifier = identifier;
		this.description = description;
		this.label = label;
	}
}
