package io.gritlabs.probe.domain.driver.voltronic;

public class VoltronicParseException extends Throwable {

	public VoltronicParseException(String message) {

		super(message);
	}
}
