package io.gritlabs.probe.domain.driver.voltronic.protocol;

import io.gritlabs.probe.domain.driver.voltronic.VoltronicParseException;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.Arrays;

@Slf4j
@ToString
@Getter
public class QpiwsCommandResponse extends VoltronicCommandResponse {

	private static final int len = 36;
	VoltronicQpiws data = null;

	@SneakyThrows
	public QpiwsCommandResponse(byte[] response) {

		super(response);

		if (response == null || response.length == 0) {
			return;
		}

		// (    100000000000000000000000000000000000                                      CRC
		// 0x28 313030303030303030303030303030303030303030303030303030303030303030303030 15efbfbd
		byte[] buffer = Arrays.copyOfRange(response, 1, response.length - 4);

		if (buffer.length < len) {
			throw new VoltronicParseException(MessageFormat.format(
					"QpiwsCommandResponse - Invalid Data Length [{0}] expecting {1}, raw: [{2}"
					, response.length, len, new String(response)));
		}

		log.trace("QpiwsCommandResponse: parsedRaw=[{}] hex=[{}]", new String(buffer), Hex.encodeHex(buffer));

		this.data = new VoltronicQpiws(new BigInteger(new String(buffer), 2).longValue());
	}

}
