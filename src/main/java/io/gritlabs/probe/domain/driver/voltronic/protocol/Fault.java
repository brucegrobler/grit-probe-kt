package io.gritlabs.probe.domain.driver.voltronic.protocol;

import lombok.Data;

@Data
public class Fault {

	Integer identifier;

	String description;
	String label;

	public Fault(Integer identifier, String description, String label) {

		this.identifier = identifier;
		this.description = description;
		this.label = label;
	}
}
