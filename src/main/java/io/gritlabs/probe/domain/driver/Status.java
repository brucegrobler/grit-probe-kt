package io.gritlabs.probe.domain.driver;

public enum Status {

	STARTED, POLLING, DISABLED, ONLINE, OFFLINE, ERROR

}
