package io.gritlabs.probe.domain.driver.voltronic.protocol;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.var;

import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;


@Slf4j
@Data
public class VoltronicQpiws {

	private static final Integer _B3_BUS_OVERVOLTAGE_FAULT = 3;
	List<Fault>   faults   = new LinkedList<>();
	List<Warning> warnings = new LinkedList<>();
	private Integer       _B10_OVER_TEMP_WARNING                   = 10;
	private Integer       _B11_FAN_LOCKED_FAULT                    = 11;
	private Integer       _B12_BATTERY_VOLTAGE_HIGH                = 12;
	private Integer       _B13_BATTERY_LOW_WARNING                 = 13;
	private Integer       _B14_RESERVED_OVERCHARGE                 = 14;
	private Integer       _B15_BATTERY_UNDERVOLT_SHUTDOWN_WARNING  = 15;
	private Integer       _B16_RESERVED_BATTERY_DERATING_WARNING   = 16;
	private Integer       _B17_OVERLOAD_WARNING                    = 17;
	private Integer       _B18_EEPROM_FAILURE_WARNING              = 18;
	private Integer       _B19_INVERTER_OVER_CURRENT_FAULT         = 19;
	private Integer       _B1_RESERVED                             = 1;
	private Integer       _B20_INVERTER_SOFT_FAILURE_FAULT         = 20;
	private Integer       _B21_SELF_TEST_FAILURE_FAULT             = 21;
	private Integer       _B22_OP_DC_OVER_VOLTAGE_FAULT            = 22;
	private Integer       _B23_BATTERY_OPEN_FAULT                  = 23;
	private Integer       _B24_CURRENT_SENSOR_FAILURE_FAULT        = 24;
	private Integer       _B25_BATTERY_SHORT_FAULT                 = 25;
	private Integer       _B26_POWER_LIMIT_WARNING                 = 26;
	private Integer       _B27_PV_VOLTAGE_HIGH_1_WARNING           = 27;
	private Integer       _B28_MPPT_OVERLOAD_FAULT_1_WARNING       = 28;
	private Integer       _B29_MPPT_OVERLOAD_WARNING_1_WARNING     = 29;
	private Integer       _B2_INVERTER_FAULT                       = 2;
	private Integer       _B30_BATTERY_TOO_LOW_TO_CHARGE_1_WARNING = 30;
	private Integer       _B31_PV_VOLTAGE_HIGH_2_WARNING           = 31;
	private Integer       _B32_MPPT_OVERLOAD_FAULT_2_WARNING       = 32;
	private Integer       _B33_MPPT_OVERLOAD_WARNING_2_WARNING     = 33;
	private Integer       _B34_BATTERY_TOO_LOW_TO_CHARGE_2_WARNING = 34;
	private Integer       _B35_PV_VOLTAGE_HIGH_3_WARNING           = 35;
	private Integer       _B36_MPPT_OVERLOAD_FAULT_3_WARNING       = 36;
	private Integer       _B37_MPPT_OVERLOAD_WARNING_3_WARNING     = 37;
	private Integer       _B38_BATTERY_TOO_LOW_TO_CHARGE_3_WARNING = 38;
	private Integer       _B4_BUS_UNDERVOLTAGE_FAULT               = 4;
	private Integer       _B5_BUS_SOFT_FAIL_FAULT                  = 5;
	private Integer       _B6_LINE_FAIL_WARNING                    = 6;
	private Integer       _B7_OPV_SHORT_WARNING                    = 7;
	private Integer       _B8_INV_VOLTAGE_TO_LOW_FAULT             = 8;
	private Integer       _B9_INV_VOLTAGE_TO_HIGH_FAULT            = 9;
	private Long          data;
	private List<Fault>   faultDefinitions                         = Arrays.asList(new Fault(_B2_INVERTER_FAULT,
					"INVERTER_FAULT", "U00"), new Fault(_B3_BUS_OVERVOLTAGE_FAULT, "BUS_OVERVOLTAGE", "F08"),
			new Fault(_B4_BUS_UNDERVOLTAGE_FAULT, "BUS_UNDERVOLTAGE", "F52"), new Fault(_B5_BUS_SOFT_FAIL_FAULT,
					"BUS_SOFT_FAIL", "F09"), new Fault(_B8_INV_VOLTAGE_TO_LOW_FAULT, "OUTPUT_VOLTAGE_TO_LOW", "F58"),
			new Fault(_B9_INV_VOLTAGE_TO_HIGH_FAULT, "OUPUT_VOLTAGE_TO_HIGH", "F55"), new Fault(_B11_FAN_LOCKED_FAULT,
					"FAN_LOCKED", "F01"), new Fault(_B19_INVERTER_OVER_CURRENT_FAULT, "INVERTER_OVER_CURRENT", "F51"),
			new Fault(_B20_INVERTER_SOFT_FAILURE_FAULT, "INVERTER_SOFT_FAILURE", "F53"),
			new Fault(_B21_SELF_TEST_FAILURE_FAULT, "SELF_TEST_FAILURE", "U01"),
			new Fault(_B22_OP_DC_OVER_VOLTAGE_FAULT, "OP_DC_OVER_VOLTAGE", "F12"), new Fault(_B23_BATTERY_OPEN_FAULT,
					"BATTERY_OPEN", "U02"), new Fault(_B24_CURRENT_SENSOR_FAILURE_FAULT, "CURRENT_SENSOR_FAILURE",
					"F57"), new Fault(_B25_BATTERY_SHORT_FAULT, "BATTERY_SHORT", "U03"));
	private List<Warning> warningDefinitions                       = Arrays.asList(new Warning(_B1_RESERVED, "RESERVED"
					, "U00"), new Warning(_B6_LINE_FAIL_WARNING, "LINE_FAIL", "U01"),
			new Warning(_B7_OPV_SHORT_WARNING,
					"OPV_SHORT", "U02"), new Warning(_B10_OVER_TEMP_WARNING, "OVER_TEMP", "F02"),
			new Warning(_B12_BATTERY_VOLTAGE_HIGH, "BATTERY_VOLTAGE_HIGH", "F03"),
			new Warning(_B13_BATTERY_LOW_WARNING, "BATTERY_VOLTAGE_LOW", "F04"), new Warning(_B14_RESERVED_OVERCHARGE,
					"RESERVED_OVERCHARGE", "U03"), new Warning(_B15_BATTERY_UNDERVOLT_SHUTDOWN_WARNING,
					"BATTERY_UNDERVOLT_SHUTDOWN", "U04"), new Warning(_B16_RESERVED_BATTERY_DERATING_WARNING,
					"RESERVED_BATTERY_DERATING", "U05"), new Warning(_B17_OVERLOAD_WARNING, "OVERLOAD", "07"),
			new Warning(_B18_EEPROM_FAILURE_WARNING, "EEPROM_FAILURE", "F90"), new Warning(_B26_POWER_LIMIT_WARNING,
					"POWER_LIMIT", "U06"), new Warning(_B27_PV_VOLTAGE_HIGH_1_WARNING, "PV_VOLTAGE_HIGH_1", "U07"),
			new Warning(_B28_MPPT_OVERLOAD_FAULT_1_WARNING, "MPPT_OVERLOAD_FAULT_1", "U08"),
			new Warning(_B29_MPPT_OVERLOAD_WARNING_1_WARNING, "MPPT_OVERLOAD_WARNING_1", "U09"),
			new Warning(_B30_BATTERY_TOO_LOW_TO_CHARGE_1_WARNING, "BATTERY_TOO_LOW_TO_CHARGE_1", "U10"),
			new Warning(_B31_PV_VOLTAGE_HIGH_2_WARNING, "PV_VOLTAGE_HIGH_2", "U11"),
			new Warning(_B32_MPPT_OVERLOAD_FAULT_2_WARNING, "MPPT_OVERLOAD_FAULT_2", "U12"),
			new Warning(_B33_MPPT_OVERLOAD_WARNING_2_WARNING, "MPPT_OVERLOAD_WARNING_2", "U13"),
			new Warning(_B34_BATTERY_TOO_LOW_TO_CHARGE_2_WARNING, "BATTERY_TOO_LOW_TO_CHARGE_2", "U14"),
			new Warning(_B35_PV_VOLTAGE_HIGH_3_WARNING, "PV_VOLTAGE_HIGH_3", "U15"),
			new Warning(_B36_MPPT_OVERLOAD_FAULT_3_WARNING, "MPPT_OVERLOAD_FAULT_3", "U16"),
			new Warning(_B37_MPPT_OVERLOAD_WARNING_3_WARNING, "MPPT_OVERLOAD_WARNING_3", "U17"),
			new Warning(_B38_BATTERY_TOO_LOW_TO_CHARGE_3_WARNING, "BATTERY_TOO_LOW_TO_CHARGE_3", "U18"));

	public VoltronicQpiws(long value) {

		BitSet bits = convert(value);

		for (Fault fault : faultDefinitions) {
			if (isSet(this.data, fault.identifier)) {
				log.trace("Fault: {}", fault);
				faults.add(fault);
			}
		}
		for (Warning warning : warningDefinitions) {
			if (isSet(data, warning.identifier)) {
				log.trace("Warning: {}", warning);
				warnings.add(warning);
			}
		}
	}

	public BitSet convert(Long value) {

		BitSet bits = new BitSet();
		Integer index = 0;
		while (value != 0L) {
			if (value % 2L != 0L) {
				bits.set(index);
			}
			++index;
			value = value >> 1;
		}
		return bits;
	}

	private Boolean isSet(Long value, Integer position) {

		var bitSet = convert(value);

		return bitSet.get(position);
	}
}

