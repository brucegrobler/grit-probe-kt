package io.gritlabs.probe.domain.driver.voltronic.protocol;

public class QmodCommandRequest extends VoltronicCommandRequest {

	public static final String QMOD = "QMOD";

	public QmodCommandRequest() {

		super(QMOD);
	}
}
