package io.gritlabs.probe.domain.driver.voltronic.protocol;

public class QmodMode {

	public static final String BATTERY_MODE      = "Battery Mode";
	public static final String FAULT_MODE        = "Fault Mode";
	public static final String LINE_MODE         = "Line Mode";
	public static final String POWER_ON_MODE     = "Power On Mode";
	public static final String POWER_SAVING_MODE = "Power Saving Mode";
	public static final String STANDBY_MODE      = "Standby Mode";
}
