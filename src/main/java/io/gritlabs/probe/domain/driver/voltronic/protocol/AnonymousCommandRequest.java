package io.gritlabs.probe.domain.driver.voltronic.protocol;

public class AnonymousCommandRequest extends VoltronicCommandRequest {

	public AnonymousCommandRequest(String command) {

		super(command);
	}
}
