package io.gritlabs.probe.domain.driver.voltronic.protocol;

import lombok.Data;

@Data
public class VoltronicQpigs {

	String acChargeEnabled;
	Double batteryCapacity;
	Double batteryChargeCurrent;
	Double batteryCurrent;
	Double batteryDischargeCurrent;
	Double batteryPower;
	String batteryVoltageOffset;
	Double busDcVoltage;

	Double batteryVoltage;
	String chargingFloatMode;
	String chargingStatusScc1;
	String configurationUpdated;
	String deviceChargingStatus;
	String deviceStatus1;
	Double batteryVoltageScc1;
	String deviceStatus2;
	Double gridFrequency;
	Double gridVoltage;
	Double heatsinkTemperature;
	Double loadApparentPower;
	Double loadFrequency;
	String batteryVoltageSteadyUnderCharge;
	Double loadPercent;
	Double loadPower;
	String chargingStatus;
	String loadStatus;
	String eepromVersion;
	Double mpptPower1;
	Double loadVoltage;
	Double mpptCurrent1;
	String powerSwitchState;
	String reserved1;
	Double mpptVoltage1;
	String sbuPriorityVersion;
	String sccFirmwareUpdated;

	public VoltronicQpigs(Double gridVoltage, Double gridFrequency, Double loadVoltage, Double loadApparentPower,
	                      Double loadPower, Double loadPercent, Double busDcVoltage, Double batteryVoltage,
	                      Double batteryChargeCurrent, Double batteryCapacity, Double heatsinkTemperature,
	                      Double mpptCurrent1, Double mpptVoltage1, Double batteryVoltageScc1,
	                      Double batteryDischargeCurrent, String deviceStatus1, String sbuPriorityVersion,
	                      String configurationUpdated, String sccFirmwareUpdated, String loadStatus,
	                      String batteryVoltageSteadyUnderCharge, String chargingStatusScc1, String acChargeEnabled,
	                      String chargingStatus, String batteryVoltageOffset, String eepromVersion, Double mpptPower1,
	                      String deviceStatus2, String chargingFloatMode, String powerSwitchState, String reserved1,
	                      Double loadFrequency, String deviceChargingStatus, Double batteryCurrent,
	                      Double batteryPower) {

		this.gridVoltage = gridVoltage;
		this.gridFrequency = gridFrequency;
		this.loadVoltage = loadVoltage;
		this.loadApparentPower = loadApparentPower;
		this.loadPower = loadPower;
		this.loadPercent = loadPercent;
		this.busDcVoltage = busDcVoltage;
		this.batteryVoltage = batteryVoltage;
		this.batteryChargeCurrent = batteryChargeCurrent;
		this.batteryCapacity = batteryCapacity;
		this.heatsinkTemperature = heatsinkTemperature;
		this.mpptCurrent1 = mpptCurrent1;
		this.mpptVoltage1 = mpptVoltage1;
		this.batteryVoltageScc1 = batteryVoltageScc1;
		this.batteryDischargeCurrent = batteryDischargeCurrent;
		this.deviceStatus1 = deviceStatus1;
		this.sbuPriorityVersion = sbuPriorityVersion;
		this.configurationUpdated = configurationUpdated;
		this.sccFirmwareUpdated = sccFirmwareUpdated;
		this.loadStatus = loadStatus;
		this.batteryVoltageSteadyUnderCharge = batteryVoltageSteadyUnderCharge;
		this.chargingStatusScc1 = chargingStatusScc1;
		this.acChargeEnabled = acChargeEnabled;
		this.chargingStatus = chargingStatus;
		this.batteryVoltageOffset = batteryVoltageOffset;
		this.eepromVersion = eepromVersion;
		this.mpptPower1 = mpptPower1;
		this.deviceStatus2 = deviceStatus2;
		this.chargingFloatMode = chargingFloatMode;
		this.powerSwitchState = powerSwitchState;
		this.reserved1 = reserved1;
		this.loadFrequency = loadFrequency;
		this.deviceChargingStatus = deviceChargingStatus;
		this.batteryCurrent = batteryCurrent;
		this.batteryPower = batteryPower;
	}
}
