package io.gritlabs.probe.domain.driver.voltronic;

public class VoltronicReadException extends Throwable {

	public VoltronicReadException(String message) {

		super(message);
	}
}
