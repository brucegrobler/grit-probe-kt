package io.gritlabs.probe.domain.driver.voltronic.protocol;

public class VoltronicCommandResponse {

	protected byte[] response;

	public VoltronicCommandResponse(byte[] response) {

		this.response = response;
	}
}
