package io.gritlabs.probe.domain.driver.voltronic.protocol;

public class QpiriCommandRequest extends VoltronicCommandRequest {


	public static final String QPIRI = "QPIRI";

	public QpiriCommandRequest() {

		super(QPIRI);
	}
}
