package io.gritlabs.probe.domain.driver.voltronic.protocol;

import lombok.Data;

@Data
public class VoltronicQpiri {

	Double  batteryBulkVoltage;
	Double  batteryFloatVoltage;
	Double  batteryReDischargeVoltage;
	String  batteryType; // 0: AGM, 1: Flooded, 2: User
	String  chargerSourcePriority; // 0: Utiility, 1: Solar, 2: Solar +  Utility, 3: Only Solar
	Integer chargingCurrentMax;
	Double  gridRatingCurrent;
	Double  batteryRatingVoltage;
	Double  batteryReChargeVoltage;
	Double  batteryUnderVoltage;
	Double  gridRatingVoltage;
	String  inputVoltageRange; // 0: Appliance, 1: UPS
	Integer loadOutputRatingActivePower;
	Integer gridChargingCurrentMax;
	Integer loadOutputRatingApparentPower;
	Double  loadOutputRatingCurrent;
	Double  loadOutputRatingFrequency;
	Double  loadOutputRatingVoltage;
	Integer parallelMaxNum;
	String  machineType; // 00: Grid Tie, 01: Off grid, 10: Hybrid, 11: Off grid with 2 mppt, 20: off grid w/ 3
	String  outputMode; // 00: single machine output01: parallel output02: Phase 1 of 3 Phase output03: Phase 2
	String  outputSourcePriority; // 0: Utility, 1: Solar, 2: SBU
	String  pvOkCondition;
	String  pvPowerBalance;
	// mppt
	String  topology; // 0: Transformless, 1: transformer


	public VoltronicQpiri(Double gridRatingVoltage, Double gridRatingCurrent, Double loadOutputRatingVoltage,
	                      Double loadOutputRatingFrequency, Double loadOutputRatingCurrent,
	                      Integer loadOutputRatingApparentPower, Integer loadOutputRatingActivePower,
	                      Double batteryRatingVoltage, Double batteryReChargeVoltage, Double batteryUnderVoltage,
	                      Double batteryBulkVoltage, Double batteryFloatVoltage, String batteryType,
	                      Integer gridChargingCurrentMax, Integer chargingCurrentMax, String inputVoltageRange,
	                      String outputSourcePriority, String chargerSourcePriority, Integer parallelMaxNum,
	                      String machineType, String topology, String outputMode, Double batterReDischargeVoltage,
	                      String pvOkCondition, String pcPowerBalance) {

		this.gridRatingVoltage = gridRatingVoltage;
		this.gridRatingCurrent = gridRatingCurrent;
		this.loadOutputRatingVoltage = loadOutputRatingVoltage;
		this.loadOutputRatingFrequency = loadOutputRatingFrequency;
		this.loadOutputRatingCurrent = loadOutputRatingCurrent;
		this.loadOutputRatingApparentPower = loadOutputRatingApparentPower;
		this.loadOutputRatingActivePower = loadOutputRatingActivePower;
		this.batteryRatingVoltage = batteryRatingVoltage;
		this.batteryReChargeVoltage = batteryReChargeVoltage;
		this.batteryUnderVoltage = batteryUnderVoltage;
		this.batteryBulkVoltage = batteryBulkVoltage;
		this.batteryFloatVoltage = batteryFloatVoltage;
		this.batteryType = batteryType;
		this.gridChargingCurrentMax = gridChargingCurrentMax;
		this.chargingCurrentMax = chargingCurrentMax;
		this.inputVoltageRange = inputVoltageRange;
		this.outputSourcePriority = outputSourcePriority;
		this.chargerSourcePriority = chargerSourcePriority;
		this.parallelMaxNum = parallelMaxNum;
		this.machineType = machineType;
		this.topology = topology;
		this.outputMode = outputMode;
		this.batteryReDischargeVoltage = batteryReDischargeVoltage;
		this.pvOkCondition = pvOkCondition;
		this.pvPowerBalance = pvPowerBalance;

	}
}
