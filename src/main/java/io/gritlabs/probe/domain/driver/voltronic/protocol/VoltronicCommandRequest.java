package io.gritlabs.probe.domain.driver.voltronic.protocol;

import io.gritlabs.probe.util.Util;

import java.nio.ByteBuffer;

public class VoltronicCommandRequest {

	public static final String POP_02 = "POP02";
	protected           String command;

	public VoltronicCommandRequest(String command) {

		this.command = command;
	}

	private ByteBuffer constructCommandBuffer(String command) {

		ByteBuffer commandBuffer = ByteBuffer.allocate(command.length() + 3);

		// Strange bug in voltronic firmware, crc incorrect for POP02.. must be 0xE20B instead of valid 0xE20A...
		byte[] crc;
		if (command.contains(POP_02)) {
			crc = ByteBuffer.allocate(4).putInt(0xe20b).array();
		} else {
			crc = ByteBuffer
					.allocate(4)
					.putInt(Util.calculateCrc16(command.getBytes())).array();
		}

		commandBuffer.put(command.getBytes());
		commandBuffer.put(crc, 2, 2);
		commandBuffer.put((byte) 0x0d);
		return commandBuffer;
	}

	public ByteBuffer toByteBuffer() {

		return constructCommandBuffer(command);
	}
}
