package io.gritlabs.probe.domain.driver.voltronic;

import io.gritlabs.probe.domain.driver.PollingDriver;
import io.gritlabs.probe.domain.driver.Status;
import io.gritlabs.probe.domain.driver.voltronic.protocol.*;
import io.gritlabs.probe.domain.repository.CoulombRepository;
import io.gritlabs.probe.domain.repository.entity.CoulombEntity;
import io.gritlabs.probe.domain.service.EventDispatcherServiceImpl;
import io.gritlabs.probe.domain.service.dto.MeasurementDto;
import io.gritlabs.probe.domain.service.dto.PointDto;
import io.gritlabs.probe.domain.service.dto.PointTypeDto;
import io.gritlabs.probe.domain.service.model.DeviceAction;
import io.gritlabs.probe.domain.service.model.DeviceModel;
import io.gritlabs.probe.domain.service.model.EventListener;
import io.gritlabs.probe.domain.service.model.EventRequestModel;
import io.gritlabs.probe.util.CancelableReader;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@Scope("prototype")
public class VoltronicMksDriver extends PollingDriver implements EventListener {

	private static final CharSequence      COMMA                     = ", ";
	private final        Object            cumulativeMutex           = new Object();
	private final        ExecutorService   workerExecutor            =
			Executors.newCachedThreadPool(new CustomizableThreadFactory("voltronic" +
					"-evnt-"));
	private              Double            batteryBankDod            = 10.0;
	private              String            batteryBankName           = device.getName();
	private              Double            batteryCapacityAh         = 0.0;
	private              Double            batteryLossMultiplier     = 0.96;
	@Autowired
	private              CoulombRepository coulombRepository;
	private              Double            inverterBaseLoad          = 0.0;
	private              Double            loadLossMultiplier        = 1.04;
	private              Double            mpptLossMultiplier        = 0.92;
	private              Boolean           newGridPowerAlgorithm     = true;
	private              String            port                      = "/dev/hidraw0";
	private              Boolean           queryForFaultsAndWarnings = false;
	private              boolean           reading                   = false;

	public VoltronicMksDriver(DeviceModel device) {

		super(device);
		this.setUpdated(ZonedDateTime.now());
	}

	private Double calculateBatteryPowerReal(QpigsCommandResponse qpigsResponse) {

		if (qpigsResponse.getData().getBatteryPower() > 0) {
			return qpigsResponse.getData().getBatteryPower() + inverterBaseLoad;
		}
		if (qpigsResponse.getData().getBatteryPower() == 0.00) {
			return 0.00;
		}

		return qpigsResponse.getData().getBatteryPower() - inverterBaseLoad;
	}

	private void calculateCumulative(QpigsCommandResponse qpigsResponse, List<PointDto> points) {

		synchronized (cumulativeMutex) {
			log.trace("[${device.name}] Locked cumulative calc");

			if (batteryCapacityAh == 0.0) {
				// Counter disabled, set batteryCapacityAh to enable
				log.debug("[${device.name}] Coulomb counter disabled, set batteryCapacityAh (greater than 0) to " +
						"enable" +
						".");
				return;
			}

			var storedCoulombs = coulombRepository.findByName(batteryBankName);

			if (!storedCoulombs.isPresent()) {
				log.trace("[${device.name}] with $batteryBankName new StoredCoulombs");
				var coulombEntity = new CoulombEntity(UUID.randomUUID(), ZonedDateTime.now(), ZonedDateTime.now(),
						null, batteryBankName, batteryCapacityAh, 100.0);
				storedCoulombs = Optional.of(coulombRepository.save(coulombEntity));
			}

			var current = 0.0;
			var charging = false;

			if (qpigsResponse.getData().getBatteryPower() >= 0) {
				log.trace("[${device.name}] $batteryBankName Battery Charging");
				current = qpigsResponse.getData().getBatteryCurrent();
				charging = true;
			} else if (qpigsResponse.getData().getBatteryPower() <= 0) {
				log.trace("[${device.name}] $batteryBankName Battery Discharging");
				current = qpigsResponse.getData().getBatteryCurrent() * -1;
			}

			val coulombs = current * ((device.getInterval() / 1000) / 3600);

			var currentAmpHours = coulombs + storedCoulombs.get().getAmpHours();

			// Calibrate
			if (charging && (currentAmpHours > batteryCapacityAh)) {
				log.info("[${device.name}] Battery $batteryBankName Auto-Calibrated");
				currentAmpHours = batteryCapacityAh;
			}

			storedCoulombs.get().setUpdated(ZonedDateTime.now());
			storedCoulombs.get().setAmpHours(currentAmpHours);
			storedCoulombs.get().setSoc((currentAmpHours / batteryCapacityAh) * 100);
			coulombRepository.save(storedCoulombs.get());

			log.trace("[${device.name}] $batteryBankName Current: $current, Interval: ${device.interval}, " +
					"StoredCoulombs" + ".ampHours: " + "${storedCoulombs.get().ampHours} Coulombs: $coulombs " +
					"Percent:" +
					" " +
					"${storedCoulombs.get().soc}");

			var estimatedTimeToRechargedMin = 0.0;
			var estimatedTimeToDischargedMin = 0.0;

			if (charging) {
				estimatedTimeToRechargedMin = (batteryCapacityAh - currentAmpHours) / current * 60;
			} else {
				estimatedTimeToDischargedMin = (currentAmpHours / (current * -1)) * 60;
			}

			points.add(new PointDto("BatteryBankName", batteryBankName, PointTypeDto.STRING));
			points.add(new PointDto("BatteryChargeAh", storedCoulombs.get().getAmpHours(), PointTypeDto.DECIMAL));
			points.add(new PointDto("BatterySoC", storedCoulombs.get().getSoc(), PointTypeDto.DECIMAL));
			points.add(new PointDto("BatteryCapacityAh", batteryCapacityAh, PointTypeDto.DECIMAL));
			points.add(new PointDto("BatteryTimeToDischargedMin", estimatedTimeToDischargedMin, PointTypeDto.DECIMAL));
			points.add(new PointDto("BatteryTimeToRechargedMin", estimatedTimeToRechargedMin, PointTypeDto.DECIMAL));
			points.add(new PointDto("BatteryDepthOfDischarge", batteryBankDod, PointTypeDto.DECIMAL));
		}
	}

	private Double calculateGridPower(QpigsCommandResponse qpigsResponse, QmodCommandResponse qmodResponse,
	                                  QpiriCommandResponse qpiriResponse) {

		Double gridPower = 0.0;
		if (newGridPowerAlgorithm) {
			// Grid Power equals loads + battery power (=/-) - mppt. the remainder is coming from the grid. however,
			// if losses are not accounted for accurately, the grid power will drift slightly.
			Double loads =
					((inverterBaseLoad + qpigsResponse.getData().getLoadPower() * loadLossMultiplier) + (qpigsResponse.getData().getBatteryPower())) * batteryLossMultiplier;
			gridPower = loads - (qpigsResponse.getData().getMpptPower1() * mpptLossMultiplier);

			// Set to zero in certain known circumstances.
			if (!qmodResponse.getMode().equals(QmodMode.LINE_MODE)) {
				gridPower = 0.0;
			}
		} else {
			if (qmodResponse.getMode().equals(QmodMode.LINE_MODE)) {
				if (qpiriResponse.getData().getChargerSourcePriority() == "0") {
					gridPower =
							inverterBaseLoad + qpigsResponse.getData().getLoadPower() + qpigsResponse.getData().getBatteryPower();
				} else {
					gridPower = inverterBaseLoad + qpigsResponse.getData().getLoadPower();
				}
			}
		}
		return gridPower;
	}

	private Double calculateLoadPowerReal(QpigsCommandResponse qpigsResponse) {

		if (qpigsResponse.getData().getLoadPower() > 0) {
			return qpigsResponse.getData().getLoadPower() + inverterBaseLoad;
		}

		if (qpigsResponse.getData().getLoadPower() == 0.00) {
			return 0.00;
		}

		return qpigsResponse.getData().getLoadPower() - inverterBaseLoad;

	}

	public void destroy() {

		this.reading = false;
	}

	@PostConstruct
	public void init() {

		if (!device.isEnabled()) {
			setStatus(Status.DISABLED);
			return;
		}

		registerActions();

		this.port = device.getParameters().getOrDefault("port", port).toString();
		if (device.getParameters().containsKey("batteryCapacityAh")) {
			this.batteryCapacityAh = Double.valueOf(device.getParameters().get("batteryCapacityAh").toString());
		}
		if (device.getParameters().containsKey("baseLoad")) {
			this.inverterBaseLoad = Double.valueOf(device.getParameters().get("baseLoad").toString());
		}
		if (device.getParameters().containsKey("batteryBankName")) {
			this.batteryBankName = device.getParameters().get("batteryBankName").toString();
		}
		if (device.getParameters().containsKey("batteryBankDod")) {
			this.batteryBankDod = Double.valueOf(device.getParameters().get("batteryBankDod").toString());
		}
		if (device.getParameters().containsKey("newGridPowerAlgorithm")) {
			this.newGridPowerAlgorithm =
					Boolean.valueOf(device.getParameters().get("newGridPowerAlgorithm").toString());
		}
		if (device.getParameters().containsKey("batteryLossMultiplier")) {
			this.batteryLossMultiplier =
					Double.valueOf(device.getParameters().get("batteryLossMultiplier").toString());
		}
		if (device.getParameters().containsKey("loadLossMultiplier")) {
			this.loadLossMultiplier = Double.valueOf(device.getParameters().get("loadLossMultiplier").toString());
		}
		if (device.getParameters().containsKey("mpptLossMultiplier")) {
			this.mpptLossMultiplier = Double.valueOf(device.getParameters().get("mpptLossMultiplier").toString());
		}
		if (device.getParameters().containsKey("queryForFaultsAndWarnings")) {
			this.queryForFaultsAndWarnings =
					Boolean.valueOf(device.getParameters().get("queryForFaultsAndWarnings").toString());
		}
	}

	public void onEventReceived(EventRequestModel eventRequest) {

		String topic = eventRequest.getTopic();
		if (topic.contains(device.getName())) {
			if (topic.endsWith("/request")) {
				if (!topic.contains("/response")) {
					val split = topic.split("/");
					val command = split[split.length - 2];

					log.info("[${device.name}] Voltronic Event: Topic=[" + eventRequest.getTopic() + "] " +
							"Payload=[" + eventRequest.getPayload() + "] Command=[" + command + "]");
					log.debug("[${device.name}] Voltronic Instance  [" + this + "]");

					Future result = null;

					try {
						result = workerExecutor.submit(() -> {
							try {

								val acquireLimit = 5;
								var count = 0;
								while (!this.device.getDriver().acquire() || count >= acquireLimit) {
									Thread.sleep(50);
									count++;
								}

								if (count >= acquireLimit) {
									new EventRequestModel(topic.replace("request", "response"), "ERROR could not " +
											"acquire lock.");
								}

								val queryResponse = query(new AnonymousCommandRequest(eventRequest.getPayload()));
								new EventRequestModel(
										topic.replace("request", "response"),
										queryResponse.toString()
								);
								log.info("[" + device.getName() + "] Voltronic Event Response: " + queryResponse);

								EventDispatcherServiceImpl.getEventBus().post(queryResponse);
							} catch (VoltronicReadException | InterruptedException e) {
								new EventRequestModel(topic.replace("request", "response"), "ERROR " + e.getMessage());
							}
						});
						result.get(device.getTimeout(), TimeUnit.MILLISECONDS);

					} catch (TimeoutException e) {
						log.error("[" + device.getName() + "] Voltronic - Event Worker Timeout: [" + device.getName() +
								"] [" + e.getClass().getSimpleName() + "] (" + e.getMessage());
						result.cancel(true);
					} catch (ExecutionException e) {
						throw new RuntimeException(e);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					} finally {
						device.getDriver().release();
					}
				}
			}
		}
	}

	public void process() throws VoltronicReadException {

		if (!device.isEnabled()) {
			return;
		}

		Long start = Instant.now().toEpochMilli();
		LinkedList<PointDto> points = new LinkedList<>();

		QmodCommandResponse qmodResponse = query(new QmodCommandRequest());
		points.add(new PointDto("InverterMode", qmodResponse.getMode(), PointTypeDto.STRING));

		QpigsCommandResponse qpigsResponse = query(new QpigsCommandRequest());

		Double loadPowerReal = calculateLoadPowerReal(qpigsResponse);
		val batteryPowerReal = calculateBatteryPowerReal(qpigsResponse);

		points.add(new PointDto("GridVolts", qpigsResponse.getData().getGridVoltage(), PointTypeDto.DECIMAL));
		points.add(new PointDto("GridFrequency", qpigsResponse.getData().getGridFrequency(), PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadVoltage", qpigsResponse.getData().getLoadVoltage(), PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadFrequency", qpigsResponse.getData().getLoadFrequency(), PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadPower", qpigsResponse.getData().getLoadPower(), PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadPowerReal", loadPowerReal, PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadPercent", qpigsResponse.getData().getLoadPercent(), PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryVolts", qpigsResponse.getData().getBatteryVoltage(), PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryAmps", qpigsResponse.getData().getBatteryCurrent(), PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryPower", qpigsResponse.getData().getBatteryPower(), PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryPowerReal", batteryPowerReal, PointTypeDto.DECIMAL));
		points.add(new PointDto("BatterySocEstimate", qpigsResponse.getData().getBatteryCapacity(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("Temperature", qpigsResponse.getData().getHeatsinkTemperature(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("MpptVolts", qpigsResponse.getData().getMpptVoltage1(), PointTypeDto.DECIMAL));
		points.add(new PointDto("MpptPower", qpigsResponse.getData().getMpptPower1(), PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadVolts", qpigsResponse.getData().getLoadVoltage(), PointTypeDto.DECIMAL));
		points.add(new PointDto("AcChargeEnabled", qpigsResponse.getData().getAcChargeEnabled(), PointTypeDto.INT32));
		points.add(new PointDto("BatteryChargeCurrent", qpigsResponse.getData().getBatteryChargeCurrent(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryDischargeCurrent", qpigsResponse.getData().getBatteryDischargeCurrent(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryVoltsOffset", qpigsResponse.getData().getBatteryVoltageOffset(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryVoltsScc1", qpigsResponse.getData().getBatteryVoltageScc1(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadVolts", qpigsResponse.getData().getBatteryVoltageSteadyUnderCharge(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BusDcVoltage", qpigsResponse.getData().getBusDcVoltage(), PointTypeDto.DECIMAL));
		points.add(new PointDto("ChargingFloatMode", qpigsResponse.getData().getChargingFloatMode(),
				PointTypeDto.INT32));
		points.add(new PointDto("ChargingStatus", qpigsResponse.getData().getChargingStatus(), PointTypeDto.INT32));
		points.add(new PointDto("ConfigurationUpdated", qpigsResponse.getData().getConfigurationUpdated(),
				PointTypeDto.INT32));
		points.add(new PointDto("EepromVersion", qpigsResponse.getData().getEepromVersion(), PointTypeDto.STRING));
		points.add(new PointDto("PowerSwitchState", qpigsResponse.getData().getPowerSwitchState(),
				PointTypeDto.INT32));
		points.add(new PointDto("SbuPriorityVersion", qpigsResponse.getData().getSbuPriorityVersion(),
				PointTypeDto.INT32));

		val qpiriResponse = query(new QpiriCommandRequest());

		points.add(new PointDto("GridRatingVoltage", qpiriResponse.getData().getGridRatingVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("GridRatingCurrent", qpiriResponse.getData().getGridRatingCurrent(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadOutputRatingVoltage", qpiriResponse.getData().getLoadOutputRatingVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadOutputRatingFrequency", qpiriResponse.getData().getLoadOutputRatingFrequency(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadOutputRatingCurrent", qpiriResponse.getData().getLoadOutputRatingCurrent(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("LoadOutputRatingApparentPower",
				qpiriResponse.getData().getLoadOutputRatingApparentPower(), PointTypeDto.INT32));
		points.add(new PointDto("LoadOutputRatingActivePower",
				qpiriResponse.getData().getLoadOutputRatingActivePower(), PointTypeDto.INT32));
		points.add(new PointDto("BatteryRatingVoltage", qpiriResponse.getData().getBatteryRatingVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryReChargeVoltage", qpiriResponse.getData().getBatteryReChargeVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryUnderVoltage", qpiriResponse.getData().getBatteryUnderVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryBulkVoltage", qpiriResponse.getData().getBatteryBulkVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryFloatVoltage", qpiriResponse.getData().getBatteryFloatVoltage(),
				PointTypeDto.DECIMAL));
		points.add(new PointDto("BatteryType", qpiriResponse.getData().getBatteryType(), PointTypeDto.STRING));
		points.add(new PointDto("GridChargingCurrentMax", qpiriResponse.getData().getGridChargingCurrentMax(),
				PointTypeDto.INT32));
		points.add(new PointDto("ChargingCurrentMax", qpiriResponse.getData().getChargingCurrentMax(),
				PointTypeDto.INT32));
		points.add(new PointDto("InputVoltageRange", qpiriResponse.getData().getInputVoltageRange(),
				PointTypeDto.STRING));
		points.add(new PointDto("OutputSourcePriority", qpiriResponse.getData().getOutputSourcePriority(),
				PointTypeDto.STRING));
		points.add(new PointDto("ChargerSourcePriority", qpiriResponse.getData().getChargerSourcePriority(),
				PointTypeDto.STRING));
		points.add(new PointDto("ParallelMaxNum", qpiriResponse.getData().getParallelMaxNum(), PointTypeDto.INT32));
		points.add(new PointDto("MachineType", qpiriResponse.getData().getMachineType(), PointTypeDto.STRING));
		points.add(new PointDto("Topology", qpiriResponse.getData().getTopology(), PointTypeDto.STRING));
		points.add(new PointDto("OutputMode", qpiriResponse.getData().getOutputMode(), PointTypeDto.STRING));
		points.add(new PointDto("PvOkCondition", qpiriResponse.getData().getPvOkCondition(), PointTypeDto.STRING));
		points.add(new PointDto("PvPowerBalance", qpiriResponse.getData().getPvPowerBalance(), PointTypeDto.STRING));

		val gridPower = calculateGridPower(qpigsResponse, qmodResponse, qpiriResponse);
		points.add(new PointDto("GridPower", gridPower, PointTypeDto.DECIMAL));

		if (queryForFaultsAndWarnings) {
			val qpiwsResponse = query(new QpiwsCommandRequest());
			points.add(new PointDto("Faults", qpiwsResponse.getData().getFaults().stream().map(fault -> {
				return fault.getLabel();
			}).collect(Collectors.joining(COMMA)), PointTypeDto.STRING));

			points.add(new PointDto("Warnings", qpiwsResponse.getData().getWarnings().stream().map(warning -> {
				return warning.getLabel();
			}).collect(Collectors.joining(COMMA)), PointTypeDto.STRING));
		}
		val end = Instant.now().toEpochMilli();
		calculateCumulative(qpigsResponse, points);
		telemetry.setMeasurement(new MeasurementDto(end - start, Instant.now().toEpochMilli(), points));

	}

	private QpiwsCommandResponse query(QpiwsCommandRequest qpiwsCommandRequest) throws VoltronicReadException {

		return new QpiwsCommandResponse(query(qpiwsCommandRequest.toByteBuffer()));
	}

	private QpiriCommandResponse query(QpiriCommandRequest qpiriCommandRequest) throws VoltronicReadException {

		return new QpiriCommandResponse(query(qpiriCommandRequest.toByteBuffer()));
	}

	private QmodCommandResponse query(QmodCommandRequest qmodCommandRequest) throws VoltronicReadException {

		return new QmodCommandResponse(query(qmodCommandRequest.toByteBuffer()));
	}

	private QpigsCommandResponse query(QpigsCommandRequest command) throws VoltronicReadException {

		return new QpigsCommandResponse(query(command.toByteBuffer()));
	}

	@Synchronized
	private byte[] query(ByteBuffer commandBuffer) throws VoltronicReadException {

		try (FileOutputStream outputStream = new FileOutputStream(port)) {
			try {
				log.debug("VoltronicMKS: Write: [" + commandBuffer.array().length + "] [UTF8] [" + commandBuffer.array() + "]");
				log.debug("VoltronicMKS: Write: [" + commandBuffer.array().length + "] [ HEX] [0x"
						+ Hex.encodeHex((commandBuffer.array())).toString().toUpperCase(Locale.getDefault()) + "]");
				outputStream.write(commandBuffer.array());
				outputStream.flush();
			} catch (Exception e) {
				log.error("VoltronicMKS: Query Exception [" + new String(commandBuffer.array()) + "] - " + e.getMessage(), e);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		log.debug("VoltronicMKS: Write Complete, Preparing Read");

		try (FileInputStream inputStream = new FileInputStream(port)) {

			byte[] buffer;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,
					StandardCharsets.UTF_8));

			CancelableReader reader = new CancelableReader(bufferedReader);

			new Thread(() -> {
				try {
					Thread.sleep(device.getTimeout());
					reader.cancelRead();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}).start();

			while (reading) {
				buffer = reader.readLine().getBytes();
				if (buffer.length == 0) {
					break;
				}

				log.debug("VoltronicMKS: Read [" + buffer.length + "] [UTF8] [" + new String(buffer) + "]");
				log.debug("VoltronicMKS: Read [" + buffer.length + "] [HEX] [0x" + Hex.encodeHexString(buffer) + "]");

				if (((char) ((int) buffer[0])) != '(') {
					throw new VoltronicReadException("Invalid start bytes.");
				}

				if (new String(buffer).contains("NAK")) {
					throw new VoltronicReadException("Invalid response (NAK).");
				}
				log.debug("VoltronicMKS: Read Complete");
				return buffer;
			}
			throw new VoltronicReadException("Read Timeout");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private AnonymousCommandResponse query(AnonymousCommandRequest anonymousCommandRequest) throws VoltronicReadException {

		return new AnonymousCommandResponse(query(anonymousCommandRequest.toByteBuffer()));
	}

	private void registerActions() {

		device.registerActions(
				Arrays.asList(
						new DeviceAction(
								"Charge Utility Only",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/PCP/request",
										device.getInstanceId(), device.getName()), "PCP00", "[PCP00] Charge from " +
								"Utility ONLY"),
						new DeviceAction("Charge PV First",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/PCP/request",
										device.getInstanceId(), device.getName()), "PCP01", "[PCP01] Charge from " +
								"Solar" +
								" First (if Voc > 0)"),
						new DeviceAction("Charge Solar & Utility",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/PCP/request",
										device.getInstanceId(), device.getName()), "PCP02", "[PCP02] Charge from " +
								"Solar" +
								" & Utility"),
						new DeviceAction("Charge Solar Only",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/PCP/request",
										device.getInstanceId(), device.getName()), "PCP03", "[PCP03] Charge from " +
								"Solar" +
								" ONLY"),
						new DeviceAction("Load Utility Only",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/POP/request",
										device.getInstanceId(), device.getName()), "POP00", "[POP00] Load from " +
								"Utility" +
								" First"),
						new DeviceAction("Load Solar First",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/POP/request",
										device.getInstanceId(), device.getName()), "POP01", "[POP01] Load Solar " +
								"First"),
						new DeviceAction("Load SBU",
								MessageFormat.format("grit/probe/local/{0}/{1}/command/POP/request",
										device.getInstanceId(), device.getName()), "POP02", "[POP02] Load SBU")
				));
	}

	public void validate() {

	}
}

