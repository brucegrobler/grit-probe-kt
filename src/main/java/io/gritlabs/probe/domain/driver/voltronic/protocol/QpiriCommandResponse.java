package io.gritlabs.probe.domain.driver.voltronic.protocol;

import io.gritlabs.probe.domain.driver.voltronic.VoltronicParseException;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.Arrays;

@Slf4j
@ToString
@Getter
public class QpiriCommandResponse extends VoltronicCommandResponse {

	public static final int len = 94;

	// Computer: QPIRI<CRC><cr>Device: (N1N2N3.N5N7N8.N10N12N13N14.N16N18N19.N21N23N24
	// .N26N28N29N30N31N33N34N35N36N38N39.N41N43N44.N46N48N49.N51N53N54.N56N58N59.N61N63N65N66N68N69N70N72 N74
	// N76N78N80N81N83 N85 N87N88.N90 N92 N94<CRC><cr>

	// (230.0 21.7 230.0 50.0
	// 21.7 5000 4000 48.0
	// 46.0 42.0 58.4 54.4
	// 2 40 060 0
	// 1 1 6 01
	// 0 1 54.0
	// 0 1�"

	VoltronicQpiri data = null;

	@SneakyThrows
	public QpiriCommandResponse(byte[] response) {

		super(response);

		if (response == null || response.length == 0) {
			return;
		}

		byte[] buffer = Arrays.copyOfRange(response, 1, response.length);

		if (buffer.length < len) {
			throw new VoltronicParseException(MessageFormat.format(
					"QpiriCommandResponse - Invalid Data Length [{0}] expecting {1}, " +
							"raw: [{2}]", response.length, len, new String(response)));
		}

		String[] data = new String(buffer).split(" ");

		log.trace("QpiriCommandResponse: raw=[{}]", data);
		// 230.0 21.7 230.0 50.0
		// 21.7 5000 4000 48.0
		// 46.0 42.0 58.4 54.4
		// 2 40 060 0
		// 1 1 6 01
		// 0 1 54.0
		// 0 1

		Double gridRatingVoltage = Double.valueOf(data[0]);
		Double gridRatingCurrent = Double.valueOf(data[1]);
		Double loadOutputRatingVoltage = Double.valueOf(data[2]);
		Double loadOutputRatingFrequency = Double.valueOf(data[3]);
		Double loadOutputRatingCurrent = Double.valueOf(data[4]);
		Integer loadOutputRatingApparentPower = Integer.valueOf(data[5]);
		Integer loadOutputRatingActivePower = Integer.valueOf(data[6]);
		Double batteryRatingVoltage = Double.valueOf(data[7]);
		Double batteryReChargeVoltage = Double.valueOf(data[8]);
		Double batteryUnderVoltage = Double.valueOf(data[9]);
		Double batteryBulkVoltage = Double.valueOf(data[10]);
		Double batteryFloatVoltage = Double.valueOf(data[11]);
		String batteryType = data[12];
		Integer gridChargingCurrentMax = Integer.valueOf(data[13]);
		Integer chargingCurrentMax = Integer.valueOf(data[14]);
		String inputVoltageRange = data[15];
		String outputSourcePriority = data[16];
		String chargerSourcePriority = data[17];
		Integer parallelMaxNum = Integer.valueOf(data[18]);
		String machineType = data[19];
		String topology = data[20];
		String outputMode = data[21];
		String pvOkCondition = data[22];
		String pcPowerBalance = data[23];

		Double batterReDischargeVoltage = 0d; // DON'T KNOW WHERE THIS COMES FROM …

		this.data = new VoltronicQpiri(
				gridRatingVoltage, gridRatingCurrent,
				loadOutputRatingVoltage, loadOutputRatingFrequency,
				loadOutputRatingCurrent, loadOutputRatingApparentPower,
				loadOutputRatingActivePower, batteryRatingVoltage,
				batteryReChargeVoltage, batteryUnderVoltage,
				batteryBulkVoltage, batteryFloatVoltage,
				batteryType, gridChargingCurrentMax,
				chargingCurrentMax, inputVoltageRange,
				outputSourcePriority, chargerSourcePriority,
				parallelMaxNum, machineType,
				topology, outputMode,
				batterReDischargeVoltage, pvOkCondition,
				pcPowerBalance);

	}
}
