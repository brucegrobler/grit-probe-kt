package io.gritlabs.probe.domain.driver.voltronic.protocol;

import java.text.MessageFormat;
import java.util.Arrays;

public class AnonymousCommandResponse extends VoltronicCommandResponse {

	public AnonymousCommandResponse(byte[] response) {

		super(response);
	}

	public String toString() {

		return MessageFormat.format("{0}", Arrays.toString(Arrays.copyOfRange(response, 1, response.length - 2)));
	}
}
