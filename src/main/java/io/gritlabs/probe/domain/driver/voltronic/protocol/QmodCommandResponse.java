package io.gritlabs.probe.domain.driver.voltronic.protocol;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class QmodCommandResponse extends VoltronicCommandResponse {

	private String mode = "NA";

	public QmodCommandResponse(byte[] response) {

		super(response);
	}

	public void init() {

		String buffer = new String(response);

		log.trace("----> [$buffer]");

		if (buffer.contains("P")) {
			mode = QmodMode.POWER_ON_MODE;
		}
		if (buffer.contains("S")) {
			mode = QmodMode.STANDBY_MODE;
		}
		if (buffer.contains("L")) {
			mode = QmodMode.LINE_MODE;
		}
		if (buffer.contains("B")) {
			mode = QmodMode.BATTERY_MODE;
		}
		if (buffer.contains("F")) {
			mode = QmodMode.FAULT_MODE;
		}
		if (buffer.contains("H")) {
			mode = QmodMode.POWER_SAVING_MODE;
		}
	}

}

