package io.gritlabs.probe.domain.driver.voltronic.protocol;

import io.gritlabs.probe.domain.driver.voltronic.VoltronicParseException;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.Arrays;

@Slf4j
@ToString
@Getter
public class QpigsCommandResponse extends VoltronicCommandResponse {

//    Device: (N1N2N3.N5 N7N 8.N10 N12N13N14.N 16 N18N19.N21 N23N24N25N26 N28N29N30 N31 N33N34N35
//    N37 N38N39 N41N42.N44N45 N47N48N49 N51N52N53 N55N56N57 N58 N60N61N62N 63 N65N66N67.N69
//    N71 N72.N74N75 N77N 78N79N80N81 b83 b84b85b86b87b88b89b90 N92N93 N95N96 N98N99N100N101N102
//    b104b 105b106 <CRC><cr>

	//    217.6 50.0 217.6 50.0
//    0413 0338 008 472
//    58.40 018 095 0067
//    0000 000.0 00.00 00000
//    00010101 00 00 00000
//    010

	VoltronicQpigs data = null;

	@SneakyThrows
	public QpigsCommandResponse(byte[] response) {

		super(response);

		if (response == null || response.length == 0) {
			return;
		}

		byte[] buffer = Arrays.copyOfRange(response, 1, response.length);

		if (buffer.length < 106) {
			throw new VoltronicParseException(
					MessageFormat.format(
							"QpigsCommandResponse - Invalid Data Length [{0}] expecting 106, raw: [{1}]",
							buffer.length, new String(response)));
		}

		String[] data = new String(buffer).split(" ");

		log.trace("QpigsCommandResponse: raw=[{}]", data);
//        000.0, 00.0, 230.0, 50.0,
//        0436, 0322, 008, 396,
//        49.70, 000, 075, 0051,
//        0000, 000.0, 00.00, 00006,
//        00010000, 00, 00, 00000,
//        010

		Double gridVoltage = Double.valueOf(data[0]);
		Double gridFrequency = Double.valueOf(data[1]);
		Double loadVoltage = Double.valueOf(data[2]);
		Double loadFrequency = Double.valueOf(data[3]);
		Double loadApparentPower = Double.valueOf(data[4]);
		Double loadPower = Double.valueOf(data[5]);
		Double loadPercent = Double.valueOf(data[6]);
		Double busDcVoltage = Double.valueOf(data[7]);
		Double batteryVoltage = Double.valueOf(data[8]);
		Double batteryChargeCurrent = Double.valueOf(data[9]);
		Double batteryCapacity = Double.valueOf(data[10]);
		Double heatsinkTemperature = Double.valueOf(data[11]);
		Double mpptCurrent = Double.valueOf(data[12]);
		Double mpptVoltage = Double.valueOf(data[13]);
		Double batteryDischargeCurrent = Double.valueOf(data[15]);
		Double batteryCurrent = batteryChargeCurrent + batteryDischargeCurrent;

		Double batteryPower;
		if (batteryDischargeCurrent > 0) {
			batteryPower = (-1 * (batteryDischargeCurrent) * batteryVoltage); // return negative number.
		} else {
			batteryPower = (batteryChargeCurrent * batteryVoltage);
		}

		String deviceStatus1 = data[16];

		String sbuPriorityVersion = deviceStatus1.substring(0, 1);
		String configurationUpdated = deviceStatus1.substring(1, 2);
		String sccFirmwareUpdated = deviceStatus1.substring(2, 3);
		String loadStatus = deviceStatus1.substring(3, 4);
		String batteryVoltageSteadyUnderCharge = deviceStatus1.substring(4, 5);
		String deviceChargingStatus = deviceStatus1.substring(5, 7);

		String chargingStatusScc1 = "0";
		if ("110".equals(deviceChargingStatus)) {
			chargingStatusScc1 = "1";
		}

		String acChargeEnabled = "0";
		if ("101".equals(deviceChargingStatus)) {
			acChargeEnabled = "1";
		}

		if ("111".equals(deviceChargingStatus)) {
			acChargeEnabled = "1";
			chargingStatusScc1 = "1";
		}

		String batteryVoltageOffset = data[17];
		String eepromVersion = data[18];
		Double mpptPower1 = Double.valueOf(data[19]);
		String deviceStatus2 = data[20];
		String chargingFloatMode = deviceStatus2.substring(0, 1);
		String powerSwitchState = deviceStatus2.substring(1, 2);
		String reserved1 = deviceStatus2.substring(2, 3);

		this.data = new VoltronicQpigs(
				gridVoltage,
				gridFrequency,
				loadVoltage,
				loadApparentPower,
				loadPower,
				loadPercent,
				busDcVoltage,
				batteryVoltage,
				batteryChargeCurrent,
				batteryCapacity,
				heatsinkTemperature,
				mpptCurrent,
				mpptVoltage,
				0.0,
				batteryDischargeCurrent,
				deviceStatus1,
				sbuPriorityVersion,
				configurationUpdated,
				sccFirmwareUpdated,
				loadStatus,
				batteryVoltageSteadyUnderCharge,
				chargingStatusScc1,
				acChargeEnabled,
				chargingStatusScc1,
				batteryVoltageOffset,
				eepromVersion,
				mpptPower1,
				deviceStatus2,
				chargingFloatMode,
				powerSwitchState,
				reserved1,
				loadFrequency,
				deviceChargingStatus,
				batteryCurrent,
				batteryPower
		);
	}

}
