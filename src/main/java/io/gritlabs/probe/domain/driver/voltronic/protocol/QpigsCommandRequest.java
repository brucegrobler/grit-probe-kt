package io.gritlabs.probe.domain.driver.voltronic.protocol;

public class QpigsCommandRequest extends VoltronicCommandRequest {

	public static final String QPIGS = "QPIGS";

	public QpigsCommandRequest() {

		super(QPIGS);
	}
}
