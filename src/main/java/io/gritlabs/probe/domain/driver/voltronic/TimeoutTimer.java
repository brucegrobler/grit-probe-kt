package io.gritlabs.probe.domain.driver.voltronic;

import io.gritlabs.probe.domain.driver.DriverTimeout;

import java.util.TimerTask;

public class TimeoutTimer extends TimerTask {

	String name;
	String port;
	Long   timeout;

	public void run() {

		throw new DriverTimeout("${name} Timeout [${port}] [${timeout}ms]");
	}
}
