package io.gritlabs.probe.domain.driver;

public class DriverTimeout extends RuntimeException {

	public DriverTimeout(String message) {
		super(message);
	}
}
