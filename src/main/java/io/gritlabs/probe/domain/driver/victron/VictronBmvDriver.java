package io.gritlabs.probe.domain.driver.victron;

import io.gritlabs.probe.domain.driver.PollingDriver;
import io.gritlabs.probe.domain.service.dto.MeasurementDto;
import io.gritlabs.probe.domain.service.dto.PointDto;
import io.gritlabs.probe.domain.service.dto.PointTypeDto;
import io.gritlabs.probe.domain.service.model.DeviceModel;
import lombok.SneakyThrows;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static io.gritlabs.probe.domain.driver.Status.DISABLED;

@Scope("prototype")
@Component
@Slf4j
public class VictronBmvDriver extends PollingDriver {

	private final ExecutorService workerExecutor = Executors.newCachedThreadPool(new CustomizableThreadFactory("bmv" +
			"-worker" +
			"-"));
	private       String          port           = "/dev/ttyUSB0";

	public VictronBmvDriver(DeviceModel device) {

		super(device);
	}

	public void destroy() {
		// Nothing to destroy
	}

	@PostConstruct
	public void init() {

		if (!device.isEnabled()) {
			setStatus(DISABLED);
			return;
		}

		this.port = device.getParameters().get("port").toString();
		log.debug("VictronBmv: Acquiring file [${port}] handle...");
		File bmvFile = new File(port);
		log.debug("VictronBmv: Acquired file [${port}] handle");

		workerExecutor.submit(() -> {
			while (device.isEnabled()) {
				Long start = Instant.now().toEpochMilli();
				LinkedList<PointDto> points = new LinkedList<PointDto>();
				List<String> lines = read(bmvFile);
				parse(lines, points, start);
				log.debug("VictronBmv: Complete.");
			}
		});
		log.debug("VictronBmv: Initialized.");
	}

	private void parse(List<String> lines, List<PointDto> points, Long start) {

		try {
			for (String line : lines) {

				if (StringUtils.isBlank(line)) {
					continue;
				}

				log.debug("VictronBmv: Parse - content=[$line]  len=[${line.length}]");

				if (StringUtils.isNotBlank(line)) {
					String[] labelValue = line.split(BmvProtocol.DELIMITER);
					if (labelValue.length > 1) {
						String label = labelValue[0];
						String value = labelValue[1];

						if (label.equalsIgnoreCase(BmvProtocol.CHECKSUM)) {
							// Dont add checksum values.,
							continue;
						}

						val mappedLabel = BmvProtocol.mapLabel(label);

						/* For reference...

                        if (!mappedLabel.isNullOrEmpty()) {
                            val parsedValue = BmvProtocol.parseValue(label, value)
                            if (points.stream().anyMatch { it.name == mappedLabel }.not()) {
                                points.add(PointDto(mappedLabel, parsedValue.first, parsedValue.second))
                            } else {
                                points.removeIf { it.name == mappedLabel }
                                points.add(PointDto(mappedLabel, parsedValue.first, parsedValue.second))
                            }
                        }
						 */


						if (StringUtils.isNotBlank(mappedLabel)) {
							Pair<Object, PointTypeDto> parsedValue = BmvProtocol.parseValue(label, value);

							if (points.stream().noneMatch(pointDto -> pointDto.getName().equalsIgnoreCase(mappedLabel))) {
								points.add(new PointDto(mappedLabel, parsedValue.getLeft(), parsedValue.getRight()));
							} else {
								points.removeIf(it -> it.getName().equalsIgnoreCase(mappedLabel));
								points.add(new PointDto(mappedLabel, parsedValue.getLeft(), parsedValue.getRight()));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("VictronBmv - Parse Exception - {}", e.getMessage(), e);
		} finally {
			val end = Instant.now().toEpochMilli();
			telemetry.setMeasurement(new MeasurementDto(
					end - start,
					Instant.now().toEpochMilli(),
					points));
		}
	}

	@Synchronized
	public void process() {
		// This driver is passive, reads the file continuously.
	}

	@SneakyThrows
	private List<String> read(File bmvFile) {

		BufferedReader reader = new BufferedReader(new FileReader(bmvFile));
		int chunkCount = 0;
		log.debug("VictronBmv: [$chunkCount] Reading (60 lines)...");
		List<String> lines = reader.lines().limit(60).collect(Collectors.toList());

		return lines;
	}

	public void validate() {
		// TODO
	}

}

