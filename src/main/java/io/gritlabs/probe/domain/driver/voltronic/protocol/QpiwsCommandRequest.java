package io.gritlabs.probe.domain.driver.voltronic.protocol;


public class QpiwsCommandRequest extends VoltronicCommandRequest {

	public static final String QPIWS = "QPIWS";

	public QpiwsCommandRequest() {

		super(QPIWS);
	}
}
