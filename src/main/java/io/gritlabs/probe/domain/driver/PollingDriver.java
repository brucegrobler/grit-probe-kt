package io.gritlabs.probe.domain.driver;

import io.gritlabs.probe.domain.service.dto.MeasurementDto;
import io.gritlabs.probe.domain.service.dto.TelemetryDto;
import io.gritlabs.probe.domain.service.model.DeviceModel;
import lombok.Data;
import lombok.SneakyThrows;

import java.time.ZonedDateTime;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Data
public abstract class PollingDriver {

	protected TelemetryDto telemetry;

	protected DeviceModel device;

	public PollingDriver(DeviceModel device) {

		this.device = device;
		telemetry = new TelemetryDto(device.getInstanceId(), device.getName(),
				new MeasurementDto(0, 0));
	}

	Semaphore     semaphore                = new Semaphore(1);
	AtomicLong    semaphoreExhaustionCount = new AtomicLong(0);
	String        status                   = Status.STARTED.toString();
	ZonedDateTime updated;

	@SneakyThrows
	public Boolean acquire() {

		return semaphore.tryAcquire(1, 10, TimeUnit.SECONDS);
	}

	public abstract void destroy();

	public abstract void init();

	public abstract void process() throws Throwable;

	public void release() {

		semaphore.release();
	}

	public void setStatus(Status status) {
		this.status = status.toString();
		telemetry.status = status.toString();
	}

	public abstract void validate();
}
