package io.gritlabs.probe.domain.driver;

import io.gritlabs.probe.domain.service.dto.MeasurementDto;
import io.gritlabs.probe.domain.service.dto.PointDto;
import io.gritlabs.probe.domain.service.dto.PointTypeDto;
import io.gritlabs.probe.domain.service.model.DeviceAction;
import io.gritlabs.probe.domain.service.model.DeviceModel;
import io.gritlabs.probe.domain.service.model.EventListener;
import io.gritlabs.probe.domain.service.model.EventRequestModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.LinkedList;

@Slf4j
@Component
@Scope("prototype")
public class SimulationDriver extends PollingDriver implements EventListener {

	ZonedDateTime updated = ZonedDateTime.now();

	public SimulationDriver(DeviceModel device) {

		super(device);
		this.updated = ZonedDateTime.now();
	}

	public void destroy() {
		log.info("SIMULATION - DESTROY {}", device.getName());
	}

	@PostConstruct
	public void init() {
		log.info("SIMULATION - INIT " + device.getName());

		if (device.getName() == "simulator1") {
			device.registerActions(
					Arrays.asList(
							new DeviceAction(
									"Test1",
									MessageFormat.format("{0}/{1}/event/test1/request", device.getInstanceId(),
											device.getName()),
									"value1",
									"Send a test event 1."
							),
							new DeviceAction(
									"Test2",
									MessageFormat.format("{0}/{1}/event/test2/request", device.getInstanceId(),
											device.getName()),
									"value2",
									"Send a test event 2."
							),
							new DeviceAction(
									"Test3",
									MessageFormat.format("{0}/{1}/event/test3/request", device.getInstanceId(),
											device.getName()),
									"value3",
									"Send a test event 3."
							),
							new DeviceAction(
									"Test4",
									MessageFormat.format("{0}/{1}/event/test4/request", device.getInstanceId(),
											device.getName()),
									"value4",
									"Send a test event 4."
							),
							new DeviceAction(
									"Test5",
									MessageFormat.format("{0}/{1}/event/test5/request", device.getInstanceId(),
											device.getName()),
									"value5",
									"Send a test event 5."
							),
							new DeviceAction(
									"Test6",
									MessageFormat.format("{0}/{1}/event/test6/request", device.getInstanceId(),
											device.getName()),
									"value6",
									"Send a test event 6."
							),
							new DeviceAction(
									"Test7",
									MessageFormat.format("{0}/{1}/event/test7/request", device.getInstanceId(),
											device.getName()),
									"value7",
									"Send a test event 7."
							),
							new DeviceAction(
									"Test8",
									MessageFormat.format("{0}/{1}/event/test8/request", device.getInstanceId(),
											device.getName()),
									"value8",
									"Send a test event 8."
							),
							new DeviceAction(
									"Test9",
									MessageFormat.format("{0}/{1}/event/test9/request", device.getInstanceId(),
											device.getName()),
									"value9",
									"Send a test event 9."
							),
							new DeviceAction(
									"Test10",
									MessageFormat.format("{0}/{1}/event/test10/request", device.getInstanceId(),
											device.getName()),
									"value10",
									"Send a test event 10."
							),
							new DeviceAction(
									"Test11",
									MessageFormat.format("{0}/{1}/event/test11/request", device.getInstanceId(),
											device.getName()),
									"value11",
									"Send a test event 11."
							),
							new DeviceAction(
									"Test12",
									MessageFormat.format("{0}/{1}/event/test12/request", device.getInstanceId(),
											device.getName()),
									"value12",
									"Send a test event 12."
							)
					)
			);
		}
	}

	public void onEventReceived(EventRequestModel eventRequestModel) {

		if (eventRequestModel.getTopic().contains(device.getName())) {

			log.info(
					"SIMULATION - EVENT RECEIVED - MATCHED {} Event: Topic=[{}] " +
							"Payload=[{}]", device.getName(),
					eventRequestModel.getTopic(), eventRequestModel.getPayload()
			);
		} else {
			log.info(
					"SIMULATION - EVENT RECEIVED - IGNORING {} Event: Topic=[{}] " +
							"Payload=[{}]", device.getName(), eventRequestModel.getTopic(),
					eventRequestModel.getPayload()
			);
		}
	}

	public void process() {
		if (!device.isEnabled()) {
			return;
		}

		log.info("SIMULATION - PROCESS {}", device.getName());
		simulateTelemetry();
	}

	private void simulateTelemetry() {
		// TODO GET RID OF THIS.
		Long start = Instant.now().toEpochMilli();

		LinkedList<PointDto> points = new LinkedList<PointDto>();
		points.add(new PointDto("TEST_POINT", "TESTING", PointTypeDto.STRING));
		points.add(new PointDto("TEST_POINT_1", "1", PointTypeDto.DECIMAL));
		points.add(new PointDto("TEST_POINT_2", "2", PointTypeDto.DECIMAL));
		points.add(new PointDto("TEST_POINT_3", "3", PointTypeDto.DECIMAL));
		points.add(new PointDto("TEST_POINT_4", "4", PointTypeDto.DECIMAL));
		points.add(new PointDto("TEST_POINT_5", 5.0, PointTypeDto.DECIMAL));

		telemetry.setMeasurement(new MeasurementDto(
				Instant.now().toEpochMilli() - start,
				Instant.now().toEpochMilli(),
				points));

		this.updated = ZonedDateTime.now();
	}

	public void validate() {
		log.info("SIMULATION - VALIDATE {}", device.getName());
	}
}
