package io.gritlabs.probe.domain.driver.victron;

import io.gritlabs.probe.domain.service.dto.PointTypeDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class BmvProtocol {

	public static final    String       CHECKSUM       = "Checksum";
	protected static final String       DELIMITER      = "\t";
	private static final   String       NEWLINE        = "\n";
	private static final   String       VEDIRECT_ALARM = "Alarm";
	private static final   String       VEDIRECT_BMV   = "BMV";
	private static final   String       VEDIRECT_CE    = "CE";
	private static final   String       VEDIRECT_CS    = "CS";
	private static final   String       VEDIRECT_ERR   = "ERR";
	private static final   String       VEDIRECT_FW    = "FW";
	private static final   String       VEDIRECT_H1    = "H1";
	private static final   String       VEDIRECT_H10   = "H10";
	private static final   String       VEDIRECT_H11   = "H11";
	private static final   String       VEDIRECT_H12   = "H12";
	private static final   String       VEDIRECT_H15   = "H15";
	private static final   String       VEDIRECT_H16   = "H16";
	private static final   String       VEDIRECT_H17   = "H17";
	private static final   String       VEDIRECT_H18   = "H18";
	private static final   String       VEDIRECT_H19   = "H19";
	private static final   String       VEDIRECT_H2    = "H2";
	private static final   String       VEDIRECT_H20   = "H20";
	private static final   String       VEDIRECT_H21   = "H21";
	private static final   String       VEDIRECT_H22   = "H22";
	private static final   String       VEDIRECT_H23   = "H23";
	private static final   String       VEDIRECT_H3    = "H3";
	private static final   String       VEDIRECT_H4    = "H4";
	private static final   String       VEDIRECT_H5    = "H5";
	private static final   String       VEDIRECT_H6    = "H6";
	private static final   String       VEDIRECT_H7    = "H7";
	private static final   String       VEDIRECT_H8    = "H8";
	private static final   String       VEDIRECT_H9    = "H9";
	private static final   String       VEDIRECT_HSDS  = "HSDS";
	private static final   String       VEDIRECT_I     = "I";
	private static final   String       VEDIRECT_MODE  = "MODE";
	private static final   String       VEDIRECT_P     = "P";
	private static final   String       VEDIRECT_PID   = "PID";
	private static final   String       VEDIRECT_RELAY = "Relay";
	private static final   String       VEDIRECT_SER   = "SER#";
	private static final   String       VEDIRECT_SOC   = "SOC";
	private static final   String       VEDIRECT_T     = "T";
	private static final   String       VEDIRECT_TTG   = "TTG";
	private static final   String       VEDIRECT_V     = "V";
	private final          List<String> fields         = Arrays.asList(
			VEDIRECT_V, VEDIRECT_T, VEDIRECT_I, VEDIRECT_P, VEDIRECT_CE, VEDIRECT_SOC, VEDIRECT_TTG,
			VEDIRECT_ALARM, VEDIRECT_BMV, VEDIRECT_FW, VEDIRECT_H1, VEDIRECT_H2, VEDIRECT_H3, VEDIRECT_H4,
			VEDIRECT_H5, VEDIRECT_H6, VEDIRECT_H7, VEDIRECT_H8, VEDIRECT_H9, VEDIRECT_H10, VEDIRECT_H11,
			VEDIRECT_H12, VEDIRECT_H15, VEDIRECT_H16, VEDIRECT_H17, VEDIRECT_H18, VEDIRECT_H19,
			VEDIRECT_H20, VEDIRECT_H21, VEDIRECT_H22, VEDIRECT_H23, VEDIRECT_ERR, VEDIRECT_CS,
			VEDIRECT_PID, VEDIRECT_SER, VEDIRECT_HSDS, VEDIRECT_MODE, VEDIRECT_RELAY
	);

	public static String mapLabel(String label) {

		if (VEDIRECT_V.equalsIgnoreCase(label)) {
			return "BatteryVoltage";
		}
		if (VEDIRECT_T.equalsIgnoreCase(label)) {
			return "BatteryTemperature";
		}
		if (VEDIRECT_I.equalsIgnoreCase(label)) {
			return "BatteryCurrent";
		}
		if (VEDIRECT_P.equalsIgnoreCase(label)) {
			return "BatteryPower";
		}
		if (VEDIRECT_CE.equalsIgnoreCase(label)) {
			return "ConsumedAmpHours";
		}
		if (VEDIRECT_SOC.equalsIgnoreCase(label)) {
			return "StateOfCharge";
		}
		if (VEDIRECT_TTG.equalsIgnoreCase(label)) {
			return "TimeRemainingMinutes";
		}
		if (VEDIRECT_ALARM.equalsIgnoreCase(label)) {
			return "AlarmCondition";
		}
		if (VEDIRECT_BMV.equalsIgnoreCase(label)) {
			return "ModelDescription";
		}
		if (VEDIRECT_FW.equalsIgnoreCase(label)) {
			return "FirmwareVersion";
		}
		if (VEDIRECT_H1.equalsIgnoreCase(label)) {
			return "DeepestDepthOfDischargeAmpHours";
		}
		if (VEDIRECT_H2.equalsIgnoreCase(label)) {
			return "DepthOfLastDischargeAmpHours";
		}
		if (VEDIRECT_H3.equalsIgnoreCase(label)) {
			return "DepthOfAverageDischargeAmpHours";
		}
		if (VEDIRECT_H4.equalsIgnoreCase(label)) {
			return "ChargeCyclesCount";
		}
		if (VEDIRECT_H5.equalsIgnoreCase(label)) {
			return "FullDischargeCount";
		}
		if (VEDIRECT_H6.equalsIgnoreCase(label)) {
			return "CumulativeAmpHoursDrawn";
		}
		if (VEDIRECT_H7.equalsIgnoreCase(label)) {
			return "MinBatteryVoltage";
		}
		if (VEDIRECT_H8.equalsIgnoreCase(label)) {
			return "MaxBatteryVoltage";
		}
		if (VEDIRECT_H9.equalsIgnoreCase(label)) {
			return "CurrentDischargeCycleDurationSeconds";
		}
		if (VEDIRECT_H10.equalsIgnoreCase(label)) {
			return "AutomaticSynchronizationCount";
		}
		if (VEDIRECT_H11.equalsIgnoreCase(label)) {
			return "LowMainVoltageAlarmCount";
		}
		if (VEDIRECT_H12.equalsIgnoreCase(label)) {
			return "HighMainVoltageAlarmCount";
		}
		if (VEDIRECT_H15.equalsIgnoreCase(label)) {
			return "MinAuxBatteryVoltage";
		}
		if (VEDIRECT_H16.equalsIgnoreCase(label)) {
			return "MaxAuxBatteryVoltage";
		}
		if (VEDIRECT_H17.equalsIgnoreCase(label)) {
			return "AmountDischargedEnergyKWh";
		}
		if (VEDIRECT_H18.equalsIgnoreCase(label)) {
			return "AmountChargedEnergyKWh";
		}
		if (VEDIRECT_H19.equalsIgnoreCase(label)) {
			return "YieldTotalKWh";
		}
		if (VEDIRECT_H20.equalsIgnoreCase(label)) {
			return "YieldTodayKWh";
		}
		if (VEDIRECT_H21.equalsIgnoreCase(label)) {
			return "MaxPowerTodayWatts";
		}
		if (VEDIRECT_H22.equalsIgnoreCase(label)) {
			return "YieldYesterdayKWh";
		}
		if (VEDIRECT_H23.equalsIgnoreCase(label)) {
			return "MaximumPowerYesterdayWatts";
		}
		if (VEDIRECT_ERR.equalsIgnoreCase(label)) {
			return "ErrorCode";
		}
		if (VEDIRECT_CS.equalsIgnoreCase(label)) {
			return "StateOfOperation";
		}
		if (VEDIRECT_PID.equalsIgnoreCase(label)) {
			return "ProductId";
		}
		if (VEDIRECT_SER.equalsIgnoreCase(label)) {
			return "SerialNumber";
		}
		if (VEDIRECT_HSDS.equalsIgnoreCase(label)) {
			return "DaySequenceNumber";
		}
		if (VEDIRECT_MODE.equalsIgnoreCase(label)) {
			return "DeviceMode";
		}
		if (VEDIRECT_RELAY.equalsIgnoreCase(label)) {
			return "RelayState";
		}
		return null;
	}

	public static Pair<Object, PointTypeDto> parseValue(String label, String value) {

		try {
			if (VEDIRECT_V.equalsIgnoreCase(label)) {
				return Pair.of(Double.valueOf(value) / 1000, PointTypeDto.DECIMAL); // mV to V
			}


			if (VEDIRECT_T.equalsIgnoreCase(label)) {
				if (!value.contains("-")) {
					return Pair.of(Double.valueOf(value), PointTypeDto.DECIMAL);
				}
				return Pair.of(value, PointTypeDto.STRING);
			}
			if (VEDIRECT_I.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 1000), PointTypeDto.DECIMAL);   // mA to A
			}
			if (VEDIRECT_P.equalsIgnoreCase(label)) {
				return Pair.of(Double.valueOf(value), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_CE.equalsIgnoreCase(label)) {
				return Pair.of(Double.valueOf(value), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_SOC.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 10), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H1.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 1000), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H2.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 1000), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H3.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H4.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H5.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H6.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 100), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H7.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 1000), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H8.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value) / 1000), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H9.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H10.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H11.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H12.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H15.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H16.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H17.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H18.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H19.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H20.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H21.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H22.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_H23.equalsIgnoreCase(label)) {
				return Pair.of((Double.valueOf(value)), PointTypeDto.DECIMAL);
			}
			if (VEDIRECT_ALARM.equalsIgnoreCase(label)) {
				return Pair.of((value), PointTypeDto.STRING);
			}
			if (VEDIRECT_BMV.equalsIgnoreCase(label)) {
				return Pair.of((value), PointTypeDto.STRING);
			}
			if (VEDIRECT_RELAY.equalsIgnoreCase(label)) {
				return Pair.of((value), PointTypeDto.STRING);
			}
			if (VEDIRECT_PID.equalsIgnoreCase(label)) {
				return Pair.of((value), PointTypeDto.STRING);
			}
			if (VEDIRECT_TTG.equalsIgnoreCase(label)) {
				return Pair.of((Integer.valueOf(value)), PointTypeDto.INT32);
			}
			return Pair.of(Double.valueOf(value), PointTypeDto.DECIMAL);
		} catch (Exception e) {
			log.error("VictronBmv: Protocol Error (Parsing [{}] [{}]) - {}", label, value, e.getMessage());
			return Pair.of(value, PointTypeDto.STRING);
		}
	}

}
