package io.gritlabs.probe.api;

import io.gritlabs.probe.api.dto.EventDto;
import io.gritlabs.probe.domain.service.EventDispatcherService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventApiControllerImpl implements EventApiController {


	private EventDispatcherService eventService;

	public EventApiControllerImpl(EventDispatcherService eventService) {

		this.eventService = eventService;
	}

	@PostMapping(path = "/api/event/queue", consumes = "application/json")
	public ResponseEntity<Void> postEvent(@RequestBody EventDto event) {

		eventService.dispatch(event.getTopic(), event.getPayload());
		return ResponseEntity.accepted().build();
	}

}
