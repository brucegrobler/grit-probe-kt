package io.gritlabs.probe.api;

import io.gritlabs.probe.api.dto.EventDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface EventApiController {

	public ResponseEntity<Void> postEvent(@RequestBody EventDto event);
}
