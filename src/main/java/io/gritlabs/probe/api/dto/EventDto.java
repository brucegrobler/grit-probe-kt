package io.gritlabs.probe.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EventDto {

	private String payload;
	private String topic;
}
