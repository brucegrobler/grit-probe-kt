package io.gritlabs.probe.api;

import io.gritlabs.probe.domain.service.DeviceService;
import io.gritlabs.probe.domain.service.dto.DeviceStatusDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;

@RestController
public class DeviceApiControllerImpl implements DeviceApiController {

	private DeviceService deviceService;

	public DeviceApiControllerImpl(DeviceService deviceService) {

		this.deviceService = deviceService;
	}

	@Override
	@GetMapping(path = "/api/device/status", produces = "application/json")
	public ResponseEntity<LinkedList<DeviceStatusDto>> getStatus() {

		return ResponseEntity.ok(deviceService.getDeviceStatus());
	}
}
