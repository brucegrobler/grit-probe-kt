package io.gritlabs.probe.api;

import io.gritlabs.probe.domain.service.dto.DeviceStatusDto;
import org.springframework.http.ResponseEntity;

import java.util.LinkedList;

public interface DeviceApiController {

	public ResponseEntity<LinkedList<DeviceStatusDto>> getStatus();
}
