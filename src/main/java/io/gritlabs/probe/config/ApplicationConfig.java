package io.gritlabs.probe.config;

import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import io.gritlabs.probe.util.VersionHolder;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.time.format.DateTimeFormatter;

@Configuration
@EnableScheduling
@EnableConfigurationProperties
public class ApplicationConfig {

	private String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

	@Bean
	public VersionHolder getVersionHolder(ApplicationContext context) {

		return new VersionHolder(context);
	}

	@Bean
	public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {

		return jacksonObjectMapperBuilder -> {
			Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
			builder.simpleDateFormat(dateTimeFormat);
			builder.serializers(new ZonedDateTimeSerializer(DateTimeFormatter.ISO_DATE_TIME));
		};
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {

		return builder.build();
	}

}
