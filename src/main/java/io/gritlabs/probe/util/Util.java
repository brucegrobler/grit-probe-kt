package io.gritlabs.probe.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Util {


	public static int calculateCrc16(final byte[] bytes) {
		int crc = 0xFFFF;

		for (int j = 0; j < bytes.length; j++) {
			crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
			crc ^= (bytes[j] & 0xff);
			crc ^= ((crc & 0xff) >> 4);
			crc ^= (crc << 12) & 0xffff;
			crc ^= ((crc & 0xFF) << 5) & 0xffff;
		}
		crc &= 0xffff;
		return crc;

	}

	public static String getHostname() throws UnknownHostException {
		return InetAddress.getLocalHost().getCanonicalHostName();
	}

	public static Boolean isValidCrc16(int crcValue, byte[] data) {
		if (data == null) {
			return false;
		}

		int crcActual = calculateCrc16(data);

		return crcActual == crcValue;
	}
}
