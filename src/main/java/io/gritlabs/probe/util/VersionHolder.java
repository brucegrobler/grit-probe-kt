package io.gritlabs.probe.util;

import lombok.Data;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Data
public class VersionHolder {
	private String version;

	public VersionHolder(ApplicationContext context) {

		version = context.getBeansWithAnnotation(SpringBootApplication.class)
				.entrySet().stream()
				.findFirst()
				.flatMap(entry -> Optional.ofNullable(
						entry.getValue().getClass().getPackage().getImplementationVersion()))
				.orElse("unknown");
	}

}
