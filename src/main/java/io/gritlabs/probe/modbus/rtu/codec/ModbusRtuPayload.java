package io.gritlabs.probe.modbus.rtu.codec;

import com.digitalpetri.modbus.ModbusPdu;

public class ModbusRtuPayload {

    private final ModbusPdu modbusPdu;
    private final byte      slaveId;
    private       int       crcCode;

    public ModbusRtuPayload(byte slaveId, ModbusPdu modbusPdu) {

        this.slaveId = slaveId;
        this.modbusPdu = modbusPdu;
    }

    public int getCrcCode() {

        return crcCode;
    }

    public ModbusRtuPayload setCrcCode(int crcCode) {

        this.crcCode = crcCode;
        return this;
    }

    public ModbusPdu getModbusPdu() {

        return modbusPdu;
    }

    public byte getSlaveId() {

        return slaveId;
    }
}
