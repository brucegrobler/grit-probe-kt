package io.gritlabs.probe.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
class IndexController {
	@GetMapping
	public ModelAndView view() {
		return new ModelAndView("redirect:/devices");
	}
}
