package io.gritlabs.probe.mvc;

import io.gritlabs.probe.domain.service.DeviceService;
import io.gritlabs.probe.domain.service.EventDispatcherService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/devices")
public class DevicesController {

	private final DeviceService          deviceService;
	private final EventDispatcherService eventService;

	DevicesController(DeviceService deviceService, EventDispatcherService eventService) {

		this.deviceService = deviceService;
		this.eventService = eventService;
	}

	@PostMapping("/{deviceName}/event")
	public ModelAndView submitEvent(
			@RequestParam("eventTopic") String eventTopic,
			@RequestParam("eventPayload") String eventPayload,
			@PathVariable("deviceName") String deviceName,
			RedirectAttributes redirect) {

		eventService.dispatch(eventTopic, eventPayload);
		redirect.addFlashAttribute("globalMessage", "Event Posted [${eventTopic}]");
		return new ModelAndView("redirect:/devices/" + deviceName);
	}

	@GetMapping
	public ModelAndView view() {

		return new ModelAndView("devices/list", "devices", deviceService.getDevices());
	}

	@GetMapping("/{deviceName}")
	public ModelAndView viewDevice(@PathVariable("deviceName") String deviceName) {

		return new ModelAndView("devices/view", "device", deviceService.getDevice(deviceName));
	}

}
