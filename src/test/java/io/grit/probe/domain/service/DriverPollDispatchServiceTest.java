package io.grit.probe.domain.service;

import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DriverPollDispatchServiceTest {


	@Mock
	ApplicationContext              applicationContext;


	private Logger log = LoggerFactory.getLogger(this.getClass());

	public DriverPollDispatchServiceTest() {


	}

//	@Test
	public void WHEN_scheduled_EXPECT_dispatcher_called() throws InterruptedException {

		new Timer("test").schedule(new TestTask(
						Arrays.asList(
								new Thing(1, 500L),
								new Thing(2, 300000L),
								new Thing(3, 1000L),
								new Thing(4, 900L)
						)),
				1000, 3000);


		Thread.sleep(18000000L);
	}

	private class TestTask extends TimerTask {


		private final ThreadPoolExecutor executor;
		private final List<Thing>        things;

		public TestTask(List<Thing> things) {

			this.things = things;
			this.executor = new ThreadPoolExecutor(100, 100, 5000, TimeUnit.MILLISECONDS,
					new LinkedBlockingQueue<>());
		}

		@Override
		public void run() {

			System.out.println(Thread.currentThread().getName() + " START                   " + ZonedDateTime.now());
			ZonedDateTime start = ZonedDateTime.now();
			things.stream().forEach(thing -> {
				executor.submit(() -> {
					try {
						thing.poll();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				});
			});

			ZonedDateTime end = ZonedDateTime.now();

			System.out.println(Thread.currentThread().getName()
					+ " END: " + Duration.between(start, end).getNano()/1000 + "ms" +
					"            " + ZonedDateTime.now());

		}
	}

	private class Thing {

		private final Object MUTEX = new Object();
		private final long   delay;
		private final int    thingNo;

		public Thing(int thingNo, long delay) throws InterruptedException {
			this.delay = delay;
			this.thingNo = thingNo;
		}

		public void poll() throws InterruptedException {
			synchronized (MUTEX) {
				System.out.println(Thread.currentThread().getName() + " POLLING " + thingNo);
				Thread.sleep(delay);
				System.out.println(Thread.currentThread().getName() + " DONE    " + thingNo);
			}
		}

	}
}
